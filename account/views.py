import json

from babel._compat import force_text
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.dispatch import receiver
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils.encoding import force_str, force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.generic import ListView, CreateView, UpdateView, DetailView

from account.forms import SignupForm, ProfileForm
from account.models import User
from account.tokens import account_activation_token
from shipment_list.models import ShipmentList
from two_factor.signals import user_verified


def tracker(request):
    if request.method == "POST":
        orderId = request.POST.get('orderId', '')
        email = request.POST.get('email', '')
        try:
            order = ShipmentList.objects.filter(tracking=orderId, reference_number__companyName__email__iexact=email)
            if len(order) > 0:
                update = ShipmentList.objects.first().comments.all()
                updates = []
                for item in update:
                    updates.append({'text': item.content, 'time': item.posted.strftime("%Y-%m-%d %H:%M")})
                    response = json.dumps(updates, default=str)
                return HttpResponse(response)
            else:
                return HttpResponse('{}')
        except Exception as e:
            return HttpResponse('{}')

    return render(request, 'registration/index.html')


class Home(LoginRequiredMixin, ListView):
    model = User
    template_name = "registration/home.html"
    queryset = User.objects.all()


class Register(CreateView):
    form_class = SignupForm
    template_name = "registration/register.html"

    def form_valid(self, form):
        user = form.save(commit=False)
        user.is_active = False
        user.save()
        current_site = get_current_site(self.request)
        mail_subject = 'فعال سازی اکانت'
        message = render_to_string('registration/activate_account.html', {
            'user': user,
            'domain': current_site.domain,
            'uid': force_text(urlsafe_base64_encode(force_bytes(user.pk))),
            'token': account_activation_token.make_token(user),
        })
        to_email = form.cleaned_data.get('email')
        email = EmailMessage(
            mail_subject, message, to=[to_email]
        )
        email.send()
        return HttpResponse('لینک فعال سازی به ایمیل شما ارسال شد. <a href="account/login">ورود</a>')


def activate(request, uidb64, token):
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        return render(request, "registration/actived_user.html")
    else:
        return render(request, "registration/expire_link.html")


class Profile(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = User
    form_class = ProfileForm
    template_name = "registration/profile.html"
    success_message = " تغییرات با موفقیت ذخیره شد."
    success_url = reverse_lazy("account:profile")

    def get_object(self, queryset=None):
        return User.objects.get(pk=self.request.user.pk)

    def get_form_kwargs(self):
        kwargs = super(Profile, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs


class ProfileDetailView(LoginRequiredMixin, DetailView):
    form_class = ProfileForm

    def get_object(self, queryset=None):
        pk = self.kwargs.get("pk")
        return get_object_or_404(User.objects.all(), pk=pk)

    context_object_name = "user"

    template_name = "registration/profile_detail.html"


class GoogleMap(ListView):
    template_name = "registration/map-google.html"
    queryset = User.objects.all()


class MapVector(ListView):
    template_name = "registration/vector-map.html"
    queryset = User.objects.all()


@receiver(user_verified)
def test_receiver(request, user, device, **kwargs):
    current_site = get_current_site(request)
    if device.name == 'backup':
        message = 'Hi %(username)s,\n\n' \
                  'You\'ve verified yourself using a backup device ' \
                  'on %(site_name)s. If this wasn\'t you, your ' \
                  'account might have been compromised. You need to ' \
                  'change your password at once, check your backup ' \
                  'phone numbers and generate new backup tokens.' \
                  % {'username': user.get_username(),
                     'site_name': current_site.name}
        user.email_user(subject='Backup token used', message=message)
