from django.contrib.auth.models import AbstractUser
from django.db import models
from online_users.models import OnlineUserActivity
from phonenumber_field.modelfields import PhoneNumberField


class User(AbstractUser):
    GENDER_CHOICES = (
        ('M', 'مرد'),
        ('F', 'زن'),
    )
    is_document = models.BooleanField(default=False, verbose_name="آیا در بخش اسناد است؟ ")
    is_account = models.BooleanField(default=False, verbose_name="آیا در بخش حسابداری است ؟")
    is_sales = models.BooleanField(default=False, verbose_name="آیا بازاریاب است ؟")
    is_client = models.BooleanField(default=False, verbose_name="آیا جزو مشتریان می باشد ؟")
    email = models.EmailField(unique=True, verbose_name="ایمیل")
    phone_number = PhoneNumberField(max_length=50, verbose_name="شماره تفن", null=True, blank=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    avatar = models.ImageField(verbose_name='عکس پروفایل', upload_to='static/media/images/avatars/', null=True,
                               blank=True)
    about = models.CharField(max_length=200, verbose_name="درباره من", blank=True, null=True)
    job_title = models.CharField(max_length=50, null=True, blank=True, verbose_name="عنوان شغلی")
    activity = models.OneToOneField(OnlineUserActivity, on_delete=models.CASCADE, related_name="activity", blank=True,
                                 null=True)


    def name_activity(self):
        return self.activity.user.name
