from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import User

UserAdmin.fieldsets[2][1]["fields"] = ('job_title'
                                       , 'is_active',
                                       'is_staff',
                                       'is_superuser',
                                       'is_document',
                                       'is_account',
                                       'is_sales',
                                       'is_client',
                                       'avatar',
                                       'groups',
                                       'activity',
                                       'user_permissions')

UserAdmin.list_display += ('is_superuser',
                           'is_document',
                           'is_account',
                           'is_sales',
                           'is_client',)

admin.site.register(User, UserAdmin)



