from django import forms
from django.contrib.auth.forms import UserCreationForm

from account.models import User


class SignupForm(UserCreationForm):
    email = forms.EmailField(max_length=200)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')


class ProfileForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user")
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.fields["email"].help_text = "غیر قابل تغییر"
        if not user.is_superuser:
            self.fields["email"].disabled = True
            self.fields["job_title"].disabled = True

    class Meta:
        model = User
        fields = ["username", "email", "phone_number", "first_name", "last_name"
            , "gender", "job_title", "about", "avatar"]
