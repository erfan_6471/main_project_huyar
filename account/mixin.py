from django.shortcuts import redirect


class AuthenticatedLogin():
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.is_sales or request.user.is_superuser or request.user.is_document or request.user.is_account:
                return super().dispatch(request, *args, **kwargs)
            else:
                return redirect("account:profile")
        else:
            return redirect("login")


class FieldsContactMixin():

    def dispatch(self, request, *args, **kwargs):
        self.fields = ["company_name", "slug"]
        if request.user.is_superuser:
            self.fields.append("creator")

        return super().dispatch(request, *args, **kwargs)


class FormContactValiMixin():
    def form_valid(self, form):
        if self.request.user.is_superuser:
            form.save()
        else:
            self.obj = form.save(commit=False)
            self.obj.creator = self.request.user
        return super().form_valid(form)
