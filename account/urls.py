from django.urls import path

from account.views import GoogleMap, Home, MapVector, Profile, ProfileDetailView

app_name = "account"
urlpatterns = [
    path('', Home.as_view(), name="home"),
    path('map-google', GoogleMap.as_view(), name="google_map"),
    path('vector-map', MapVector.as_view(), name="vector_map"),
    path('profile/edit', Profile.as_view(), name='profile'),
    path('profile/detail/<int:pk>', ProfileDetailView.as_view(), name="profile_detail"),
]


