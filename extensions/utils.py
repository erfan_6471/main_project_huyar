from django.utils import timezone

from extensions import jalali


def persian_number_convertor(mystr):
    number = {
        "0": "۰",
        "1": "۱",
        "2": "۲",
        "3": "۳",
        "4": "۴",
        "5": "۵",
        "6": "۶",
        "7": "۷",
        "8": "۸",
        "9": "۹",
    }
    for e, p in number.items():
        mystr = mystr.replace(e, p)

    return mystr


def Jalali_covertor(time):
    time = timezone.localtime(time)
    Jmounth = ("فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند")
    str_time = "{},{},{}".format(time.year, time.month, time.day)
    time_to_tuple = jalali.Gregorian(str_time).persian_tuple()
    time_to_list = list(time_to_tuple)
    for index, month in enumerate(Jmounth):
        if time_to_list[1] == index + 1:
            time_to_list[1] = month
            break

    output = "{}  {}  {} ساعت {}:{} ".format(
        time_to_list[2],
        time_to_list[1],
        time_to_list[0],
        time.hour,
        time.minute
    )
    return persian_number_convertor(output)
