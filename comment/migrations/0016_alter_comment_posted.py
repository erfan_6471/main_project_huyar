# Generated by Django 4.0.1 on 2022-03-18 23:27

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('comment', '0015_alter_comment_posted'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='posted',
            field=models.DateTimeField(default=datetime.datetime(2022, 3, 18, 23, 27, 35, 609236), editable=False),
        ),
    ]
