# Generated by Django 4.0.1 on 2022-03-18 23:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('comment', '0016_alter_comment_posted'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='posted',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
