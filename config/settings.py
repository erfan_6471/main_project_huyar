import os
from pathlib import Path

from django.contrib.messages import constants as messages

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-(8@rl$5#@yzxpy&2up4lzd$pzn_+*h%qx8x9$dzl4@w@%8j_!2'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Application definition
MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'
MESSAGE_TAGS = {
    messages.DEBUG: 'alert-info',
    messages.INFO: 'alert-info',
    messages.SUCCESS: 'alert-success',
    messages.WARNING: 'alert-warning',
    messages.ERROR: 'alert-danger',
}

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'account.apps.AccountConfig',
    'django.contrib.humanize',
    'contact_list.apps.ContactListConfig',
    'client_list.apps.ClientListConfig',
    'reference_list.apps.ReferenceListConfig',
    'shipment_list.apps.ShipmentListConfig',
    'calendar_event.apps.CalendarappConfig',
    'home.apps.HomeConfig',
    'invoice.apps.InvoiceConfig',
    'todo',
    'django.contrib.sites',
    'comment',
    'phonenumber_field',
    'extensions',
    'django_countries',
    'djmoney',
    'widget_tweaks',
    'crispy_forms',
    "bootstrap3",
    'star_ratings',
    'django_filters',
    'taggit',
    'mailbox2.apps.Mailbox2Config',
    'django_render_partial',
    'online_users',
    'django_otp',
    'django_otp.plugins.otp_static',
    'django_otp.plugins.otp_totp',
    'two_factor',
    'otp_yubikey',
    'django_summernote',
    'tinymce',

]
SITE_ID = 1
CRISPY_TEMPLATE_PACK = 'bootstrap3'
COMMENTS_APP = 'django_comments_xtd'
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django_session_timeout.middleware.SessionTimeoutMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'online_users.middleware.OnlineNowMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django_otp.middleware.OTPMiddleware',
    # Always include for two-factor auth
    'django_otp.middleware.OTPMiddleware',
    # Include for Twilio gateway
    'two_factor.middleware.threadlocals.ThreadLocals',
]

ROOT_URLCONF = 'config.urls'
LOGOUT_REDIRECT_URL = 'two_factor:login'
LOGIN_URL = 'two_factor:login'
LOGIN_REDIRECT_URL = 'account:home'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'

# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',  # <-- UPDATED line
        'NAME': 'huyar_tarabar',  # <-- UPDATED line
        'USER': 'huyar_tarabar',  # <-- UPDATED line
        'PASSWORD': '63bf5386',  # <-- UPDATED line
        'HOST': 'localhost',  # <-- UPDATED line
        'PORT': '3306',
    }
}
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME'  : 'db.sqlite3',
#     }
# }


# Password validation
# https://docs.djangoproject.com/en/4.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
AUTH_USER_MODEL = "account.User"
# Internationalization
# https://docs.djangoproject.com/en/4.0/topics/i18n/

LANGUAGE_CODE = 'fa-ir'
TIME_ZONE = 'Asia/Tehran'

USE_I18N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.0/howto/static-files/

STATIC_URL = '/static/'
# STATIC_ROOT = BASE_DIR / 'static'
STATICFILES_DIRS = [
    BASE_DIR / "static"]

MEDIA_URL = '/media/'
MEDIA_ROOT = BASE_DIR / 'media'

# Default primary key field type
# https://docs.djangoproject.com/en/4.0/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

STAR_RATINGS_STAR_HEIGHT = 16
STAR_RATINGS_RANGE = 5

CURRENCY_CHOICES = [('USD', 'USD $'), ('EUR', 'EUR €'), ('IRR', 'IRR ریال'), ('CNY', 'یوان 元')]
CURRENCY_DEMO_CHOICES = [('IRR', 'IRR ریال'),]

CSS_LOCATION = os.path.join(BASE_DIR, 'static/invoice/')

# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_USE_TLS = True
EMAIL_PORT = 587
EMAIL_HOST_USER = 'erfansahraei@gmail.com'
EMAIL_HOST_PASSWORD = '63bf5386'

SESSION_EXPIRE_AFTER_LAST_ACTIVITY = True
SESSION_EXPIRE_SECONDS = 10800
SESSION_TIMEOUT_REDIRECT = 'two_factor:login'
