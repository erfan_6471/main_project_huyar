from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from account.views import Register, activate, tracker
from two_factor.admin import AdminSiteOTPRequired
from two_factor.gateways.twilio.urls import urlpatterns as tf_twilio_urls
from two_factor.urls import urlpatterns as tf_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include("django.contrib.auth.urls")),
    path('shop/tracker/', tracker, name='Home'),
    path('account/', include('home.urls')),
    path('comment/', include('comment.urls')),
    path('account/', include("account.urls")),
    path('contact/', include("contact_list.urls")),
    path('', include(tf_twilio_urls)),
    path('client_list/', include("client_list.urls")),
    path('reference_list/', include("reference_list.urls")),
    path('shipment_list/', include("shipment_list.urls")),
    path('invoice/', include('invoice.urls')),
    path('mail/', include('mailbox2.urls')),
    path('todo/', include('todo.urls', namespace="todo")),
    path('calendar/', include('calendar_event.urls', namespace='calendar')),
    path('', include(tf_urls)),
    path('register/', Register.as_view(), name='register'),
    path('activate/<slug:uidb64>/<slug:token>/', activate, name='activate'),
    path('ratings/', include('star_ratings.urls', namespace='ratings')),
    path('summernote/', include('django_summernote.urls')),
    path('tinymce/', include('tinymce.urls')),
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
