from uuid import uuid4

from django.db import models
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField

from account.models import User
from extensions.utils import Jalali_covertor


class ClientList(models.Model):
    companyName = models.CharField(max_length=100, unique=True, verbose_name="نام شرکت")
    firstName = models.CharField(max_length=100, verbose_name="نام")
    lastName = models.CharField(max_length=100, verbose_name="نام خانوادگی")
    addressLine1 = models.CharField(null=True, blank=True, max_length=200, verbose_name="آدرس پستی شرکت")
    postalCode = models.CharField(null=True, blank=True, max_length=10, verbose_name="کد پستی")
    phoneNumber = PhoneNumberField(unique=True, null=False, blank=False, verbose_name="شماره موبایل")  # Here
    telephoneNumber = PhoneNumberField(null=True, blank=True, verbose_name="شماره تلفن شرکت")  # Here
    email = models.EmailField(unique=True, verbose_name="ایمیل")
    national_id = models.CharField(null=True, blank=True, max_length=100, verbose_name="شناسه ملی")
    economic_code = models.CharField(null=True, blank=True, max_length=100, verbose_name="کد اقتصادی")
    status = models.BooleanField(verbose_name="آیا جزو مشتری های فعال می باشد؟", default=True)
    position = models.IntegerField(verbose_name="اولویت")
    sales = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name="clientlist",
                              verbose_name="بازاریاب",
                              limit_choices_to={'is_sales': True})
    publish = models.DateTimeField(verbose_name="آخرین تماس با مشتری", default=timezone.now)
    description = models.CharField(max_length=200, null=True, blank=True, verbose_name="توضیحات و اطلاعات دیگر", )

    # Utility fields
    uniqueId = models.CharField(null=True, blank=True, max_length=100)
    slug = models.SlugField(max_length=200, unique=True, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    last_updated = models.DateTimeField(blank=True, null=True)

    class Meta:
        verbose_name = "لیست مشتری"
        verbose_name_plural = "لیست مشتریان"
        ordering = ["-status", "position", "-publish", "-date_created"]

    def __str__(self):
        return self.companyName

    def save(self, *args, **kwargs):
        if self.date_created is None:
            self.date_created = timezone.localtime(timezone.now())
        if self.uniqueId is None:
            self.uniqueId = str(uuid4()).split('-')[4]
            self.slug = slugify('{} {}'.format(self.companyName, self.uniqueId))

        self.slug = slugify('{} {}'.format(self.companyName, self.uniqueId))
        self.last_updated = timezone.localtime(timezone.now())

        super(ClientList, self).save(*args, **kwargs)

    def Jpublish(self):
        return Jalali_covertor(self.publish)

    Jpublish.short_description = "آخرین تماس با مشتری"


class Settings(models.Model):
    companyName = models.CharField(max_length=100, verbose_name="نام شرکت")
    sales = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name="settings",
                              verbose_name="بازاریاب",
                              limit_choices_to={'is_sales': True})
    addressLine = models.CharField(null=True, blank=True, max_length=200, verbose_name="آدرس پستی")
    postalCode = models.CharField(null=True, blank=True, max_length=10, verbose_name="کد پستی")
    national_id = models.BigIntegerField(
        blank=True, null=True)
    telephoneNumber = PhoneNumberField(null=True, blank=True, verbose_name="شماره تلفن شرکت")  # Here
    Logo = models.ImageField(default='default_logo.jpg', upload_to='static/media/logo/clientLogo/')

    # Utility fields

    class Meta:
        verbose_name = "مشخصات شرکت هویار ترابر"
        verbose_name_plural = "مشخصات شرکت هویار ترابر"

    def __str__(self):
        return '{} '.format(self.companyName)

    def get_absolute_url(self):
        return reverse("client:client_list")
