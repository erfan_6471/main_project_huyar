from django.contrib import admin
from django.contrib import messages
from django.utils.translation import ngettext

from client_list.models import ClientList, Settings


@admin.action(description='فعال کردن مشتری انتخاب شده')
def make_active(modeladmin, request, queryset):
    queryset.update(status=True)
    updated = queryset.update(status=True)
    modeladmin.message_user(request, ngettext(
        '%d دسته بندی فعال شد.',
        '%d دسته بندی ها فعال شدند.',
        updated,
    ) % updated, messages.SUCCESS)


@admin.action(description='غیر فعال شدن دسته بندی انتخاب شده')
def make_deactivate(modeladmin, request, queryset):
    queryset.update(status=False)
    updated = queryset.update(status=False)
    modeladmin.message_user(request, ngettext(
        '%d مشتری غیر فعال شد.',
        '%d مشتری ها غیر فعال شدند.',
        updated,
    ) % updated, messages.SUCCESS)


class ClientListAdmin(admin.ModelAdmin):
    list_display = ("companyName", "firstName", "lastName", "sales", "status", "phoneNumber")
    list_filter = (("status",))
    search_fields = ("companyName", "firstName", "lasttName")
    ordering = ("-status", "-position", "-publish")
    actions = [make_deactivate, make_active]


admin.site.register(ClientList, ClientListAdmin)
admin.site.register(Settings)
