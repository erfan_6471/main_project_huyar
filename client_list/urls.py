from django.urls import path

from client_list.views import Client_list, ClientCreate, ClientUpdate, Client_Detail

app_name = "client"
urlpatterns = [path("clientlist/", Client_list.as_view(), name="client_list"),
               path('client/create', ClientCreate.as_view(), name='client-create'),
               path('client/update/<int:pk>', ClientUpdate.as_view(), name='client-update'),
               path('client/detail/<int:pk>', Client_Detail.as_view(), name="client_detail"),

               ]
