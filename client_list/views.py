from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DetailView

from client_list.filters import ClientFilterList
from client_list.mixin import FieldsClientMixin, FormClientValiMixin, ClientListMixin, SalesLogin
from client_list.models import ClientList
from two_factor.views import OTPRequiredMixin


class Client_list(OTPRequiredMixin, ClientListMixin, LoginRequiredMixin, ListView):
    def get_queryset(self):
        if self.request.user.is_superuser or self.request.user.is_account or self.request.user.is_document:
            client_list = ClientList.objects.all()
        else:
            client_list = ClientList.objects.filter(sales=self.request.user)
        client_filter = ClientFilterList(self.request.GET, queryset=client_list)
        return client_filter

    template_name = "registration/client_list.html"
    context_object_name = "filter"


class ClientCreate(OTPRequiredMixin, SalesLogin, LoginRequiredMixin, FieldsClientMixin, FormClientValiMixin,
                   SuccessMessageMixin, CreateView):
    model = ClientList
    template_name = "registration/client_create.html"
    success_url = reverse_lazy("client:client_list")
    success_message = "تغییرات با موفقیت ایجاد شد"


class ClientUpdate(OTPRequiredMixin,ClientListMixin, LoginRequiredMixin, FieldsClientMixin, FormClientValiMixin,
                   SuccessMessageMixin, UpdateView):
    model = ClientList
    template_name = "registration/client_create.html"
    success_url = reverse_lazy("client:client_list")
    success_message = "تغییرات با موفقیت ایجاد شد"


class Client_Detail(OTPRequiredMixin, ClientListMixin, LoginRequiredMixin, DetailView):

    def get_object(self, queryset=None):
        pk = self.kwargs.get("pk")
        if self.request.user.is_superuser or self.request.user.is_account or self.request.user.is_document:
            return get_object_or_404(ClientList.objects.all(), pk=pk)
        else:

            return get_object_or_404(ClientList.objects.filter(sales=self.request.user), pk=pk)

    template_name = "registration/client_detail.html"
    context_object_name = "client"
