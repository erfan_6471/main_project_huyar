import django_filters

from client_list.models import ClientList


class ClientFilterList(django_filters.FilterSet):
    companyName = django_filters.CharFilter(lookup_expr='icontains', label=" نام شرکت")
    lastName = django_filters.CharFilter(lookup_expr='icontains', label="مسئول بازرگانی شرکت")
    email = django_filters.CharFilter(lookup_expr='icontains', label=" ایمیل")
    date_created = django_filters.DateRangeFilter( label="زمان ایجاد")
    year_joined__lt = django_filters.NumberFilter(field_name='date_created', lookup_expr='year__lte',
                                                  label="ایجاد بعد قبل از این سال")
    year_joined__gt = django_filters.NumberFilter(field_name='date_created', lookup_expr='year__gte',
                                                  label="ایجاد بعد از این سال")

    class Meta:
        model = ClientList
        fields = ['companyName', 'lastName', 'email', 'date_created', 'year_joined__lt', 'year_joined__gt']
