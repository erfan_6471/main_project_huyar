##################
from django.shortcuts import redirect


class ClientListMixin():
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.is_sales or request.user.is_superuser or request.user.is_account or request.user.is_document:
                return super().dispatch(request, *args, **kwargs)
            else:
                return redirect("account:profile")
        else:
            return redirect("two_factor:login")


class SalesLogin():
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.is_sales or request.user.is_superuser:
                return super().dispatch(request, *args, **kwargs)
            else:
                return redirect("account:profile")
        else:
            return redirect("login")


class FieldsClientMixin():

    def dispatch(self, request, *args, **kwargs):
        self.fields = ["companyName", "firstName", "lastName", "phoneNumber", "telephoneNumber", "email",
                       "addressLine1", "postalCode", "national_id", "economic_code", "publish", "status", "position",
                       "description"]
        if request.user.is_superuser:
            self.fields.append("sales")

        return super().dispatch(request, *args, **kwargs)


class FormClientValiMixin():
    def form_valid(self, form):
        if self.request.user.is_superuser:
            form.save()
        elif self.request.user.is_sales:
            self.obj = form.save(commit=False)
            self.obj.sales = self.request.user

        return super().form_valid(form)
