from django.urls import path

from home.views import DashboardView
from . import views
from .views.event_list import UpcomingEventsListView
from .views.other_views import deleteEvent

app_name = "calendarapp"

urlpatterns = [
    path("calender/", views.CalendarViewNew.as_view(), name="calendar"),
    path("calenders/", views.CalendarView.as_view(), name="calendars"),
    path("event/new/", views.create_event, name="event_new"),
    path("event/edit/<int:pk>/", views.EventEdit.as_view(), name="event_edit"),
    path("event/<int:event_id>/details/", views.event_details, name="event-detail"),
    path(
        "add_eventmember/<int:event_id>", views.add_eventmember, name="add_eventmember"
    ),
    path(
        "event/<int:pk>/remove",
        views.EventMemberDeleteView.as_view(),
        name="remove_event",
    ),
    path("all-event-list/", views.AllEventsListView.as_view(), name="all_events"),
    path("running-event-list/", views.RunningEventsListView.as_view(), name="running_events"),
    path("upcoming-event-list/", UpcomingEventsListView.as_view(), name="upcoming_events"),
    path("dashboard/", DashboardView.as_view(), name="dashboard"),
    path('delete/<int:id>', deleteEvent, name='delete-event'),
]
