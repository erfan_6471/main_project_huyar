from django.apps import AppConfig


class CalendarappConfig(AppConfig):
    name = 'calendar_event'
