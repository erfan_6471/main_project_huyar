from django.urls import path

from . import views

app_name = "invoice"
urlpatterns = [
    path('dashboard', views.dashboard, name='dashboard'),
    path('invoices', views.invoices, name='invoices'),
    path('products', views.products, name='products'),
    path('product/delete/<slug:slug>', views.deleteproduct, name='delete-product'),
    path('invoices/create', views.createInvoice, name='create-invoice'),
    path('invoices/create-build/<slug:slug>', views.createBuildInvoice, name='create-build-invoice'),
    path('invoices/delete/<slug:slug>', views.deleteInvoice, name='delete-invoice'),
    path('company/settings', views.companySettings, name='company-settings'),
    path('invoices/view-pdf/<slug:slug>', views.viewPDFInvoice, name='view-pdf-invoice'),
    path('invoices/view-document/<slug:slug>', views.viewDocumentInvoice, name='view-document-invoice'),
    path('invoices/email-document/<slug:slug>', views.emailDocumentInvoice, name='email-document-invoice'),
    #     demurrage

    path('demo-invoices', views.demurrage_invoices, name='demo-invoices'),
    path('container/delete/<slug:slug>', views.deletecontainer, name='delete-container'),
    path('demo-invoices/create', views.createDemoInvoice, name='create-demo-invoice'),
    path('demo-invoices/create-build/<slug:slug>', views.createBuildDemoInvoice, name='create-build-demo-invoice'),
    path('demo-invoices/delete/<slug:slug>', views.deleteDemoInvoice, name='delete-demo-invoice'),
    path('demo-invoices/view-pdf/<slug:slug>', views.viewPDFDemoInvoice, name='view-pdf-demo-invoice'),
    path('demo-invoices/view-document/<slug:slug>', views.viewDocumentDemoInvoice, name='view-document-demo-invoice'),
    path('demo-invoices/email-document/<slug:slug>', views.emailDocumentDemoInvoice,
         name='email-document-demo-invoice'),
]
