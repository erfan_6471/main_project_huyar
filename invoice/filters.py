import django_filters

from invoice.models import Demurrage, Invoice


class DemurrageFilter(django_filters.FilterSet):
    date_created = django_filters.DateRangeFilter()

    class Meta:
        model = Demurrage
        fields = ['number', 'demo_F', 'status', 'shipment__shipment_number',
                  'shipment__MBL_number', 'shipment__HBL_number',
                  'shipment__kind_shipment', 'date_created', 'shipment__container_number', 'demo_invoice_payment']


class InquiryFilter(django_filters.FilterSet):
    date_created = django_filters.DateRangeFilter()

    class Meta:
        model = Invoice
        fields = ['number', 'paymentTerms', 'status', 'client__companyName',
                  'client__email', 'client__national_id',
                  'client__lastName', 'date_created']
