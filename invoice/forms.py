from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column
from django import forms
from djmoney.forms import MoneyWidget

from client_list.models import Settings
from config import settings
from .models import *


class DateInput(forms.DateInput):
    input_type = 'date'


class ProductForm(forms.ModelForm):
    title = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'form-control mb-3', 'placeholder': 'Enter Invoice Title'}), )
    quantity = forms.CharField(
        widget=forms.TextInput(attrs={'min_length': 0, 'type': 'number', 'class': 'form-control mb-3'}))

    description = forms.CharField(
        required=True,
        label='Enter any notes for the client',
        widget=forms.Textarea(attrs={'class': 'form-control mb-3'}))

    class Meta:
        model = Product
        widgets = {
            'currency': MoneyWidget(
                amount_widget=forms.TextInput(attrs={'class': 'form-control mb-3'}),
                currency_widget=forms.Select(attrs={
                    'class': 'form-control',
                    'placeholder': 'Jumlah Usulan Dana',
                    'id': 'currency',
                },
                    choices=settings.CURRENCY_CHOICES,
                ),
            ),
            # ... Here more widget fields
        }
        fields = ['title', 'quantity', 'currency', 'description']


class InvoiceForm(forms.ModelForm):
    TERMS = [
        ('PREPAID', 'PREPAID'),
        ('COLLECT', 'COLLECT'),
    ]
    STATUS = [
        ('CURRENT', 'CURRENT'),
        ('EMAIL_SENT', 'EMAIL_SENT'),
        ('DENIED', 'DENIED'),
        ('CONFIRM', 'CONFIRM'),
    ]

    title = forms.CharField(
        required=True,
        label='Invoice Name or Title',
        widget=forms.TextInput(attrs={'class': 'form-control mb-3', 'placeholder': 'Enter Invoice Title'}), )
    paymentTerms = forms.ChoiceField(
        choices=TERMS,
        required=True,
        label='Select Payment Terms',
        widget=forms.Select(attrs={'class': 'form-control mb-3'}), )
    status = forms.ChoiceField(
        choices=STATUS,
        required=True,
        label='Change Invoice Status',
        widget=forms.Select(attrs={'class': 'form-control mb-3'}), )
    notes = forms.CharField(
        required=True,
        label='Enter any notes for the client',
        widget=forms.Textarea(attrs={'class': 'form-control mb-3'}))

    dueDate = forms.DateField(
        required=True,
        label='Invoice Due',
        widget=DateInput(attrs={'class': 'form-control mb-3'}), )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('title', css_class='form-group col-md-6'),
                Column('dueDate', css_class='form-group col-md-6'),
                css_class='form-row'),
            Row(
                Column('paymentTerms', css_class='form-group col-md-6'),
                Column('status', css_class='form-group col-md-6'),
                css_class='form-row'),
            'notes',

            Submit('submit', ' EDIT INVOICE '))

    class Meta:
        model = Invoice
        fields = ['title', 'dueDate', 'paymentTerms', 'status', 'notes']


class SettingsForm(forms.ModelForm):
    class Meta:
        model = Settings
        fields = ['companyName', 'Logo', 'addressLine', 'postalCode']


class ClientSelectForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):

        self.user = kwargs.pop('user', None)
        self.CLIENT_LIST = ClientList.objects.filter(sales=self.user)
        self.CLIENT_CHOICES = [('-----', '--Select a Client--')]

        for client in self.CLIENT_LIST:
            d_t = (client.uniqueId, client.companyName)
            self.CLIENT_CHOICES.append(d_t)

        super(ClientSelectForm, self).__init__(*args, **kwargs)

        self.fields['client'] = forms.ChoiceField(
            label='Choose a related client',
            choices=self.CLIENT_CHOICES,
            widget=forms.Select(attrs={'class': 'form-control mb-3'}), )

    class Meta:
        model = Invoice
        fields = ['client']

    def clean_client(self):
        c_client = self.cleaned_data['client']
        if c_client == '-----':
            return None
        else:
            return ClientList.objects.get(uniqueId=c_client)


# container Form
class ContainerForm(forms.ModelForm):
    SIZE = [
        ('20HD"', '20HD"'),
        ('20GP"', '20GP"'),
        ('40GP"', '40GP"'),
        ('40HC"', '40HC"'),
    ]
    container_number = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'form-control mb-3', 'placeholder': 'Enter container number'}), )

    size = forms.ChoiceField(
        choices=SIZE,
        required=True,
        label='kind of container',
        widget=forms.Select(attrs={'class': 'form-control mb-3'}), )

    discharge_time = forms.DateField(
        required=True,
        label='Invoice Due',
        widget=DateInput(attrs={'class': 'form-control mb-3'}), )

    return_time = forms.DateField(
        required=True,
        label='Invoice Due',
        widget=DateInput(attrs={'class': 'form-control mb-3'}), )

    free_time = forms.CharField(
        widget=forms.TextInput(attrs={'min_length': 0, 'type': 'number', 'class': 'form-control mb-3'}))

    is_return = forms.BooleanField(
        label='Is container return ?',
        required=False,
        initial=False
    )

    class Meta:
        model = Container
        widgets = {
            'Demurrage_amount': MoneyWidget(
                amount_widget=forms.TextInput(attrs={'class': 'form-control mb-3'}),
                currency_widget=forms.Select(attrs={
                    'class': 'form-control',
                    'placeholder': 'Jumlah Usulan Dana',
                    'id': 'Demurrage_amount',
                },
                    choices=settings.CURRENCY_CHOICES,
                ),
            ),
            'damage_amount': MoneyWidget(
                amount_widget=forms.TextInput(attrs={'class': 'form-control mb-3'}),
                currency_widget=forms.Select(attrs={
                    'class': 'form-control',
                    'placeholder': 'Jumlah Usulan Dana',
                    'id': 'damage_amount',
                },
                    choices=settings.CURRENCY_CHOICES,
                ),
            ),
            # ... Here more widget fields
        }
        fields = ['container_number', 'size', 'discharge_time', 'return_time', 'free_time', 'Demurrage_amount',
                  'damage_amount', 'is_return']


# demurrage Form

class DumarrageForm(forms.ModelForm):
    STATUS_OPTIONS = [
        ('CURRENT', 'CURRENT'),
        ('EMAIL_SENT', 'EMAIL_SENT'),
        ('OVERDUE', 'OVERDUE'),
        ('PAID', 'PAID'),
    ]

    demo_F = forms.BooleanField(
        label='Is demurrage invoice final ?',
        required=False,
        initial=False
    )

    status = forms.ChoiceField(
        choices=STATUS_OPTIONS,
        required=True,
        label='Change Invoice Status',
        widget=forms.Select(attrs={'class': 'form-control mb-3'}), )
    demo_invoice_payment = forms.BooleanField(
        label='Has demurrage invoice paid ?',
        required=False,
        initial=False
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('demo_F', css_class='form-group col-md-6'),
                css_class='form-row'),
            Row(
                Column('payment', css_class='form-group col-md-6'),
                Column('deposit', css_class='form-group col-md-6'),

                css_class='form-row'),
            Column('exchange_rate', css_class='form-group col-md-6'),
            'status',
            Column('demo_invoice_payment', css_class='form-group col-md-6'),
            Submit('submit', ' EDIT INVOICE '))

    class Meta:
        model = Demurrage
        widgets = {
            'payment': MoneyWidget(
                amount_widget=forms.TextInput(attrs={'class': 'form-control mb-3'}),
                currency_widget=forms.Select(attrs={
                    'class': 'form-control',
                    'placeholder': 'please enter payment',
                    'id': 'Demurrage_amount',
                },
                    choices=settings.CURRENCY_DEMO_CHOICES,
                ),
            ),
            'deposit': MoneyWidget(
                amount_widget=forms.TextInput(attrs={'class': 'form-control mb-3'}),
                currency_widget=forms.Select(attrs={
                    'class': 'form-control',
                    'placeholder': 'please enter deposit',
                    'id': 'damage_amount',
                },
                    choices=settings.CURRENCY_DEMO_CHOICES,
                ),
            ),
            # ... Here more widget fields
        }

        fields = ['demo_F', 'status', 'deposit', 'payment', 'exchange_rate','demo_invoice_payment']


# select shipment
class ShipmentSelectForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.SHIPMENT_LIST = ShipmentList.objects.all()
        self.SHIPMENT_CHOICES = [('-----', '--Select a Client--')]

        for shipment in self.SHIPMENT_LIST:
            d_t = (shipment.uniqueId, shipment.shipment_number)
            self.SHIPMENT_CHOICES.append(d_t)

        super(ShipmentSelectForm, self).__init__(*args, **kwargs)

        self.fields['shipment'] = forms.ChoiceField(
            label='Choose a related shipment',
            choices=self.SHIPMENT_CHOICES,
            widget=forms.Select(attrs={'class': 'form-control mb-3'}), )

    class Meta:
        model = Demurrage
        fields = ['shipment']

    def clean_shipment(self):
        c_shipment = self.cleaned_data['shipment']
        if c_shipment == '-----':
            return None
        else:
            return ShipmentList.objects.get(uniqueId=c_shipment)
