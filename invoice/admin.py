from django.contrib import admin

from .models import *

admin.site.register(Product)
admin.site.register(Invoice)
admin.site.register(Demurrage)
admin.site.register(Container)