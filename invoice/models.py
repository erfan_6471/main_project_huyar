import datetime
from uuid import uuid4

from django.db import models
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.utils import timezone
from djmoney.models.fields import MoneyField

from account.models import User
from client_list.models import ClientList
from extensions.utils import Jalali_covertor
from shipment_list.models import ShipmentList


class Invoice(models.Model):
    TERMS = [
        ('PREPAID', 'PREPAID'),
        ('COLLECT', 'COLLECT'),
    ]

    STATUS = [
        ('CURRENT', 'CURRENT'),
        ('EMAIL_SENT', 'EMAIL_SENT'),
        ('DENIED', 'DENIED'),
        ('CONFIRM', 'CONFIRM'),
    ]

    title = models.CharField(null=True, blank=True, max_length=100)
    number = models.CharField(null=True, blank=True, max_length=100)
    dueDate = models.DateField(null=True, blank=True)
    paymentTerms = models.CharField(choices=TERMS, default='COLLECT', max_length=100)
    status = models.CharField(choices=STATUS, default='CURRENT', max_length=100)
    notes = models.TextField(null=True, blank=True)

    # RELATED fields
    client = models.ForeignKey(ClientList, blank=True, null=True, on_delete=models.SET_NULL, related_name="client")

    # Utility fields
    uniqueId = models.CharField(null=True, blank=True, max_length=100)
    slug = models.SlugField(max_length=200, unique=True, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    last_updated = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return '{} {}'.format(self.number, self.uniqueId)

    def get_absolute_url(self):
        return reverse('invoice-detail', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        if self.date_created is None:
            self.date_created = timezone.localtime(timezone.now())
        if self.uniqueId is None:
            self.uniqueId = str(uuid4()).split('-')[4]
            self.slug = slugify('{} {}'.format(self.number, self.uniqueId))

        self.slug = slugify('{} {}'.format(self.number, self.uniqueId))
        self.last_updated = timezone.localtime(timezone.now())

        super(Invoice, self).save(*args, **kwargs)


class Product(models.Model):
    title = models.CharField(null=True, blank=True, max_length=100)
    description = models.TextField(null=True, blank=True)
    quantity = models.PositiveIntegerField(null=True, blank=True)
    currency = MoneyField(max_digits=14, null=True, decimal_places=2, default_currency='USD')

    # Related Fields
    invoice = models.ForeignKey(Invoice, blank=True, null=True, on_delete=models.CASCADE)

    # Utility fields
    uniqueId = models.CharField(null=True, blank=True, max_length=100)
    slug = models.SlugField(max_length=200, unique=True, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    last_updated = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return '{} {}'.format(self.title, self.uniqueId)

    def get_absolute_url(self):
        return reverse('product-detail', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        if self.date_created is None:
            self.date_created = timezone.localtime(timezone.now())
        if self.uniqueId is None:
            self.uniqueId = str(uuid4()).split('-')[4]
            self.slug = slugify('{} {}'.format(self.title, self.uniqueId))

        self.slug = slugify('{} {}'.format(self.title, self.uniqueId))
        self.last_updated = timezone.localtime(timezone.now())

        super(Product, self).save(*args, **kwargs)


# model Demurrage

class Demurrage(models.Model):
    STATUS = [
        ('CURRENT', 'CURRENT'),
        ('EMAIL_SENT', 'EMAIL_SENT'),
        ('COLLECT', 'COLLECT'),
        ('PAID', 'PAID'),
    ]
    shipment = models.ForeignKey(ShipmentList, blank=True, null=True, on_delete=models.SET_NULL,
                                 related_name="shipment")
    number = models.CharField(null=True, blank=True, max_length=100)
    demo_F = models.BooleanField(default=False)
    status = models.CharField(choices=STATUS, default='CURRENT', max_length=100)
    deposit = MoneyField(max_digits=14, null=True, decimal_places=2, default_currency='IRR')
    payment = MoneyField(max_digits=14, null=True, decimal_places=2, default_currency='IRR')
    exchange_rate = models.FloatField(null=True, blank=True, default=None)
    demo_invoice_payment = models.BooleanField(verbose_name="آیا صورتحساب دموراژ  پرداخت شده است؟  ", default=False)
    uniqueId = models.CharField(null=True, blank=True, max_length=100)
    date_created = models.DateTimeField(blank=True, null=True)
    last_updated = models.DateTimeField(blank=True, null=True)
    slug = models.SlugField(max_length=200, unique=True, blank=True, null=True)

    def __str__(self):
        return 'demo_{}'.format(self.uniqueId)

    def save(self, *args, **kwargs):
        if self.date_created is None:
            self.date_created = timezone.localtime(timezone.now())
        if self.uniqueId is None:
            self.uniqueId = str(uuid4()).split('-')[4]
            self.slug = slugify('{} {}'.format(self.number, self.uniqueId))

        self.slug = slugify('{} {}'.format(self.number, self.uniqueId))
        self.last_updated = timezone.localtime(timezone.now())

        super(Demurrage, self).save(*args, **kwargs)

    def Jpublish(self):
        return Jalali_covertor(self.date_created)


# model Container
class Container(models.Model):
    SIZE = [
        ('20HD"', '20HD"'),
        ('20GP"', '20GP"'),
        ('40GP"', '40GP"'),
        ('40HC"', '40HC"'),
    ]

    container_number = models.CharField(max_length=100)
    size = models.CharField(choices=SIZE, default='20HD', max_length=10)
    discharge_time = models.DateField(null=True, blank=True)
    return_time = models.DateField(null=True, blank=True)
    free_time = models.PositiveIntegerField(null=True, blank=True)
    Demurrage_amount = MoneyField(max_digits=14, null=True, decimal_places=2, default_currency='IRR')
    damage_amount = MoneyField(max_digits=14, null=True, decimal_places=2, default_currency='IRR')
    demurrage = models.ForeignKey(Demurrage, blank=True, null=True, on_delete=models.CASCADE, related_name="demo")
    is_return = models.BooleanField(default=False)
    uniqueId = models.CharField(null=True, blank=True, max_length=100)
    date_created = models.DateTimeField(blank=True, null=True)
    last_updated = models.DateTimeField(blank=True, null=True)
    slug = models.SlugField(max_length=200, unique=True, blank=True, null=True)

    def __str__(self):
        return 'demo_{}'.format(self.container_number)

    def save(self, *args, **kwargs):
        if self.date_created is None:
            self.date_created = timezone.localtime(timezone.now())
        if self.uniqueId is None:
            self.uniqueId = str(uuid4()).split('-')[4]
            self.slug = slugify('{} {}'.format(self.container_number, self.uniqueId))

        self.slug = slugify('{} {}'.format(self.container_number, self.uniqueId))
        self.last_updated = timezone.localtime(timezone.now())

        super(Container, self).save(*args, **kwargs)

    def demurrage_time(self):
        return (((self.return_time - self.discharge_time) + datetime.timedelta(days=1)).days) - self.free_time

    def total_demurrage_time(self):
        return (((self.return_time - self.discharge_time) + datetime.timedelta(days=1)).days)
