# Generated by Django 4.0.1 on 2022-06-01 18:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shipment_list', '0040_alter_shipmentlist_tracking'),
        ('invoice', '0002_alter_container_demurrage'),
    ]

    operations = [
        migrations.AlterField(
            model_name='demurrage',
            name='shipment',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='shipment', to='shipment_list.shipmentlist'),
        ),
    ]
