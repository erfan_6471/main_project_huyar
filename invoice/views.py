import os
from functools import wraps
from uuid import uuid4

import pdfkit as pdfkit
from django.contrib import messages
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template.loader import get_template
from djmoney.money import Money

from client_list.models import ClientList, Settings
from config import settings
from invoice.filters import DemurrageFilter, InquiryFilter
from invoice.forms import ProductForm, InvoiceForm, ClientSelectForm, ShipmentSelectForm, ContainerForm, DumarrageForm
from invoice.functions import emailInvoiceClient
from invoice.models import Invoice, Product, Demurrage, Container


def login_required(f):
    @wraps(f)
    def g(request, *args, **kwargs):
        if request.user.is_sales or request.user.is_superuser:
            return f(request, *args, **kwargs)
        else:
            return render(request, "registration/home.html")

    return g


@login_required
def dashboard(request):
    clients = ClientList.objects.filter(sales=request.user).count()
    inquiries = Invoice.objects.filter(client__sales=request.user).count()
    ConfirmInquiry = Invoice.objects.filter(client__sales=request.user, status='CONFIRM').count()
    Demurrage_invoices = Demurrage.objects.filter(shipment=True).count()
    Demo_F_invoices = Demurrage.objects.filter(shipment=True, demo_F=True).count()

    context = {}
    context['clients'] = clients
    context['inquiries'] = inquiries
    context['ConfirmInquiry'] = ConfirmInquiry
    context['Demurrage_invoices'] = Demurrage_invoices
    context['Demo_F_invoices'] = Demo_F_invoices
    return render(request, 'invoice/dashboard.html', context)


@login_required
def invoices(request):
    context = {}
    if request.user.is_superuser:
        invoices_filter = InquiryFilter(request.GET, queryset=Invoice.objects.filter(client=True))

    else:
        invoices_filter = InquiryFilter(request.GET,
                                        queryset=Invoice.objects.filter(client=True, client__sales=request.user))

    context['filter'] = invoices_filter

    return render(request, 'invoice/invoices.html', context)


@login_required
def products(request):
    context = {}
    products = Product.objects.filter(invoice__client__sales=request.user)
    context['products'] = products

    return render(request, 'invoice/products.html', context)


@login_required
def createInvoice(request):
    # create a blank invoice ....
    number = 'INV-' + str(uuid4()).split('-')[1]
    newInvoice = Invoice.objects.create(number=number)
    newInvoice.save()

    inv = Invoice.objects.get(number=number)
    return redirect('invoice:create-build-invoice', slug=inv.slug)


def createBuildInvoice(request, slug):
    # fetch that invoice
    try:
        invoice = Invoice.objects.get(slug=slug)
        pass
    except:
        messages.error(request, 'Something went wrong')
        return redirect('invoice:invoices')

    # fetch all the products - related to this invoice
    products = Product.objects.filter(invoice=invoice)

    context = {}
    context['invoice'] = invoice
    context['products'] = products

    if request.method == 'GET':
        prod_form = ProductForm()
        inv_form = InvoiceForm(instance=invoice)
        client_form = ClientSelectForm(user=request.user)
        context['prod_form'] = prod_form
        context['inv_form'] = inv_form
        context['client_form'] = client_form
        return render(request, 'invoice/create-invoice.html', context)

    if request.method == 'POST':
        prod_form = ProductForm(request.POST)
        inv_form = InvoiceForm(request.POST, instance=invoice)
        client_form = ClientSelectForm(request.POST, user=request.user, instance=invoice)

        if prod_form.is_valid():
            obj = prod_form.save(commit=False)
            obj.invoice = invoice
            obj.save()

            messages.success(request, "Invoice product added succesfully")
            return redirect('invoice:create-build-invoice', slug=slug)
        elif inv_form.is_valid and 'paymentTerms' in request.POST:
            inv_form.save()

            messages.success(request, "Invoice updated succesfully")
            return redirect('invoice:create-build-invoice', slug=slug)
        elif client_form.is_valid() and 'client' in request.POST:

            client_form.save()
            messages.success(request, "Client added to invoice succesfully")
            return redirect('invoice:create-build-invoice', slug=slug)
        else:
            context['prod_form'] = prod_form
            context['inv_form'] = inv_form
            context['client_form'] = client_form
            messages.error(request, "Problem processing your request")
            return render(request, 'invoice/create-invoice.html', context)

    return render(request, 'invoice/create-invoice.html', context)


def deleteInvoice(request, slug):
    try:
        Invoice.objects.get(slug=slug).delete()
    except:
        messages.error(request, 'Something went wrong')
        return redirect('invoice:invoices')

    return redirect('invoice:invoices')


def deleteproduct(request, slug):
    try:
        prod = Product.objects.get(slug=slug)
        invoice_slug = prod.invoice.slug
        Product.objects.get(slug=slug).delete()
    except:
        prod = Product.objects.get(slug=slug)
        invoice_slug = prod.invoice.slug
        messages.error(request, 'Something went wrong')
        return redirect('invoice:create-build-invoice', slug=invoice_slug)
    messages.success(request, "The selected product was removed.")
    return redirect('invoice:create-build-invoice', slug=invoice_slug)


def companySettings(request):
    company = Settings.objects.filter(sales=request.user)
    context = {'company': company}
    return render(request, 'invoice/company-settings.html', context)


def viewPDFInvoice(request, slug):
    # fetch that invoice
    try:
        invoice = Invoice.objects.get(slug=slug)
        pass
    except:
        messages.error(request, 'Something went wrong')
        return redirect('invoice:invoices')

    # fetch all the products - related to this invoice
    products = Product.objects.filter(invoice=invoice)

    # Get Client Settings
    p_settings = Settings.objects.filter(sales=request.user)

    # Calculate the Invoice Total
    invoiceCurrency = ''
    invoiceTotal = 0.0

    if len(products) > 0:
        for x in products:
            y = (x.quantity) * (x.currency)
            invoiceTotal += y
    else:
        invoiceTotal = ''

    context = {}
    context['invoice'] = invoice
    context['products'] = products
    context['p_settings'] = p_settings
    context['invoiceTotal'] = invoiceTotal

    return render(request, 'invoice/invoice-template.html', context)


def viewDocumentInvoice(request, slug):
    # fetch that invoice
    try:
        invoice = Invoice.objects.get(slug=slug)
        pass
    except:
        messages.error(request, 'Something went wrong')
        return redirect('invoice:invoices')

    # fetch all the products - related to this invoice
    products = Product.objects.filter(invoice=invoice)

    # Get Client Settings
    p_settings = Settings.objects.filter(sales=request.user)

    # Calculate the Invoice Total
    invoiceCurrency = ''
    invoiceTotal = 0.0

    if len(products) > 0:
        for x in products:
            y = (x.quantity) * (x.currency)
            invoiceTotal += y
    else:
        invoiceTotal = ''

    context = {}
    context['invoice'] = invoice
    context['products'] = products
    context['p_settings'] = p_settings
    context['invoiceTotal'] = invoiceTotal

    # The name of your PDF file
    filename = '{}.pdf'.format(invoice.uniqueId)

    # HTML FIle to be converted to PDF - inside your Django directory
    template = get_template('invoice/pdf-template.html')

    # Render the HTML
    html = template.render(context)

    # Options - Very Important [Don't forget this]
    options = {
        'encoding': 'UTF-8',
        'javascript-delay': '1',  # Optional
        'enable-local-file-access': None,  # To be able to access CSS
        'page-size': 'A4',
        'custom-header': [
            ('Accept-Encoding', 'gzip')
        ],
    }
    # Javascript delay is optional

    # Remember that location to wkhtmltopdf
    config_path = 'C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe'
    config = pdfkit.configuration(wkhtmltopdf=config_path)

    # IF you have CSS to add to template
    css1 = os.path.join(settings.CSS_LOCATION, 'assets', 'css', 'bootstrap.min.css')
    css2 = os.path.join(settings.CSS_LOCATION, 'assets', 'css', 'dashboard.css')

    # Create the file
    file_content = pdfkit.from_string(html, False, configuration=config, options=options)

    # Create the HTTP Response
    response = HttpResponse(file_content, content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename = {}'.format(filename)

    # Return
    return response


def emailDocumentInvoice(request, slug):
    # fetch that invoice
    try:
        invoice = Invoice.objects.get(slug=slug)
        pass
    except:
        messages.error(request, 'Something went wrong')
        return redirect('invoice:invoices')

    # fetch all the products - related to this invoice
    products = Product.objects.filter(invoice=invoice)

    # Get Client Settings
    p_settings = Settings.objects.filter(sales=request.user)

    # Calculate the Invoice Total
    invoiceTotal = 0.0
    if len(products) > 0:
        for x in products:
            y = (x.quantity) * (x.currency)
            invoiceTotal += y
    else:
        invoiceTotal = ''

    context = {}
    context['invoice'] = invoice
    context['products'] = products
    context['p_settings'] = p_settings
    context['invoiceTotal'] = invoiceTotal

    # The name of your PDF file
    filename = '{}.pdf'.format(invoice.uniqueId)

    # HTML FIle to be converted to PDF - inside your Django directory
    template = get_template('invoice/pdf-template.html')

    # Render the HTML
    html = template.render(context)

    # Options - Very Important [Don't forget this]
    options = {
        'encoding': 'UTF-8',
        'javascript-delay': '1000',  # Optional
        'enable-local-file-access': None,  # To be able to access CSS
        'page-size': 'A4',
        'custom-header': [
            ('Accept-Encoding', 'gzip')
        ],
    }
    # Javascript delay is optional

    # Remember that location to wkhtmltopdf
    config_path = 'C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe'
    config = pdfkit.configuration(wkhtmltopdf=config_path)
    # Saving the File
    filepath = os.path.join(settings.MEDIA_ROOT, 'client_invoices\\inquery')
    os.makedirs(filepath, exist_ok=True)
    pdf_save_path = filepath + filename
    # Save the PDF
    pdfkit.from_string(html, pdf_save_path, configuration=config, options=options)

    # send the emails to client
    to_email = invoice.client.email
    from_client = "HUYAR TARABAR CO"
    emailInvoiceClient(to_email, from_client, pdf_save_path)

    invoice.status = 'EMAIL_SENT'
    invoice.save()

    # Email was send, redirect back to view - invoice
    messages.success(request, "Email sent to the client succesfully")
    return redirect('invoice:create-build-invoice', slug=slug)


# demurrage view


@login_required
def demurrage_invoices(request):
    context = {}
    if request.user.is_superuser or request.user.is_document or request.user.is_sales:
        demo_invoices_filter = DemurrageFilter(request.GET, queryset=Demurrage.objects.filter(shipment=True))
        context['filter'] = demo_invoices_filter

    return render(request, 'invoice/demo-invoices.html', context)


@login_required
def createDemoInvoice(request):
    # create a blank invoice ....
    number = 'DEMO_' + str(uuid4()).split('-')[1]
    newDemoInvoice = Demurrage.objects.create(number=number)
    newDemoInvoice.save()

    Demo_inv = Demurrage.objects.get(number=number)
    return redirect('invoice:create-build-demo-invoice', slug=Demo_inv.slug)


def createBuildDemoInvoice(request, slug):
    # fetch that invoice
    try:
        demo_invoice = Demurrage.objects.get(slug=slug)
        pass
    except:
        messages.error(request, 'Something went wrong')
        return redirect('invoice:create-build-demo-invoice')

    # fetch all the products - related to this invoice
    containers = Container.objects.filter(demurrage=demo_invoice)

    context = {}
    context['demo_invoice'] = demo_invoice
    context['containers'] = containers

    if request.method == 'GET':
        container_form = ContainerForm()
        demo_inv_form = DumarrageForm(instance=demo_invoice)
        shipment_form = ShipmentSelectForm(user=request.user)
        context['container_form'] = container_form
        context['demo_inv_form'] = demo_inv_form
        context['shipment_form'] = shipment_form
        return render(request, 'invoice/create-demo-invoice.html', context)

    if request.method == 'POST':
        container_form = ContainerForm(request.POST)
        demo_inv_form = DumarrageForm(request.POST, instance=demo_invoice)
        shipment_form = ShipmentSelectForm(request.POST, user=request.user, instance=demo_invoice)

        if container_form.is_valid():
            obj = container_form.save(commit=False)
            obj.demurrage = demo_invoice
            obj.save()

            messages.success(request, "Invoice product added succesfully")
            return redirect('invoice:create-build-demo-invoice', slug=slug)
        elif demo_inv_form.is_valid and 'status' in request.POST:
            demo_inv_form.save()

            messages.success(request, "demurrage status updated succesfully")
            return redirect('invoice:create-build-demo-invoice', slug=slug)
        elif shipment_form.is_valid() and 'shipment' in request.POST:

            shipment_form.save()
            messages.success(request, "shipment added to invoice succesfully")
            return redirect('invoice:create-build-demo-invoice', slug=slug)
        else:
            context['container_form'] = container_form
            context['demo_inv_form'] = demo_inv_form
            context['shipment_form'] = shipment_form
            messages.error(request, "Problem processing your request")
            return render(request, 'invoice/create-demo-invoice.html', context)

    return render(request, 'invoice/create-demo-invoice.html', context)


def deleteDemoInvoice(request, slug):
    try:
        Demurrage.objects.get(slug=slug).delete()
    except:
        messages.error(request, 'Something went wrong')
        return redirect('invoice:demo-invoices')

    return redirect('invoice:demo-invoices')


def viewPDFDemoInvoice(request, slug):
    # fetch that invoice
    try:
        demurrage = Demurrage.objects.get(slug=slug)
        pass
    except:
        messages.error(request, 'Something went wrong')
        return redirect('invoice:demo-invoices')

    # fetch all the products - related to this invoice
    container = Container.objects.filter(demurrage=demurrage)

    # Get Client Settings
    p_settings = Settings.objects.filter(sales=request.user)

    # Calculate the Invoice Total
    invoiceamount = 0.0
    invoiceTotal = 0.0
    Taxamount = 0.0
    TaxTotal = 0.0
    Total = 0.0

    if len(container) > 0:
        for x in container:
            if demurrage.demo_F:

                if x.Demurrage_amount.currency == Money(1, 'USD').currency:
                    if demurrage.exchange_rate:
                        y = ((float(x.Demurrage_amount.amount)) * demurrage.exchange_rate) + float(
                            (x.damage_amount.amount))
                        invoiceTotal += y
                        invoiceamount += y
                    else:
                        invoiceTotal = "------"
                else:
                    y = (float(x.Demurrage_amount.amount)) + float((x.damage_amount.amount))
                    invoiceTotal += y
                    invoiceamount += y
            else:
                currency_list = []
                for currency in Container.objects.filter(demurrage=demurrage).all():
                    currency_list.append(currency.Demurrage_amount.currency)
                if Money(1, 'USD').currency in currency_list and Money(1, 'IRR').currency in currency_list:
                    invoiceTotal = "-----"
                else:
                    y = (x.Demurrage_amount)
                    invoiceTotal += y
    else:
        invoiceTotal = ''

    if len(container) > 0:
        for z in container:
            if demurrage.demo_F:
                if z.Demurrage_amount.currency == Money(1, 'USD').currency:
                    if demurrage.exchange_rate:
                        y = .09 * (((float(z.Demurrage_amount.amount)) * demurrage.exchange_rate) + float(
                            (z.damage_amount.amount)))
                        TaxTotal += y
                        Taxamount += y
                    else:
                        TaxTotal = "------"
                else:
                    y = .09 * (((float(z.Demurrage_amount.amount))) + float(
                        (z.damage_amount.amount)))
                    TaxTotal += y
                    Taxamount += y

            else:
                TaxTotal = "------"

    else:
        TaxTotal = ''

    currency_list = []
    if len(container) > 0:
        if demurrage.demo_F:
            for currency in Container.objects.filter(demurrage=demurrage).all():
                currency_list.append(currency.Demurrage_amount.currency)
            if Money(1, 'USD').currency in currency_list:
                if demurrage.exchange_rate:
                    Total = invoiceamount + Taxamount - float(demurrage.deposit.amount) - float(
                        demurrage.payment.amount)
                else:
                    Total = "-------"

            else:
                Total = invoiceamount + Taxamount - float(demurrage.deposit.amount) - float(
                    demurrage.payment.amount)
        else:
            Total = invoiceTotal


    else:
        Total = ''

    context = {}
    context['demurrage'] = demurrage
    context['container'] = container
    context['p_settings'] = p_settings
    context['invoiceTotal'] = (f"{invoiceTotal:,} ریال") if type(invoiceTotal) is float else invoiceTotal
    context['TaxTotal'] = (f"{TaxTotal:,} ریال") if type(TaxTotal) is float else TaxTotal

    if type(Total) is float and Total >= 0:
        context['Total'] = (f"{Total:,} ریال (بدهکار)")
    elif type(Total) is float and Total < 0:
        context['Total'] = (f"{Total:,} ریال (بستانکار)")
    else:
        context['Total'] = Total

    return render(request, 'invoice/invoice-demo-template.html', context)


def viewDocumentDemoInvoice(request, slug):
    # fetch that invoice
    try:
        demurrage = Demurrage.objects.get(slug=slug)
        pass
    except:
        messages.error(request, 'Something went wrong')
        return redirect('invoice:demo-invoices')

    # fetch all the products - related to this invoice
    container = Container.objects.filter(demurrage=demurrage)

    # Get Client Settings
    p_settings = Settings.objects.filter(sales=request.user)

    # Calculate the Invoice Total
    invoiceamount = 0.0
    invoiceTotal = 0.0
    Taxamount = 0.0
    TaxTotal = 0.0
    Total = 0.0

    if len(container) > 0:
        for x in container:
            if demurrage.demo_F:

                if x.Demurrage_amount.currency == Money(1, 'USD').currency:
                    if demurrage.exchange_rate:
                        y = ((float(x.Demurrage_amount.amount)) * demurrage.exchange_rate) + float(
                            (x.damage_amount.amount))
                        invoiceTotal += y
                        invoiceamount += y
                    else:
                        invoiceTotal = "------"
                else:
                    y = (float(x.Demurrage_amount.amount)) + float((x.damage_amount.amount))
                    invoiceTotal += y
                    invoiceamount += y
            else:
                currency_list = []
                for currency in Container.objects.filter(demurrage=demurrage).all():
                    currency_list.append(currency.Demurrage_amount.currency)
                if Money(1, 'USD').currency in currency_list and Money(1, 'IRR').currency in currency_list:
                    invoiceTotal = "-----"
                else:
                    y = (x.Demurrage_amount)
                    invoiceTotal += y
    else:
        invoiceTotal = ''

    if len(container) > 0:
        for z in container:
            if demurrage.demo_F:
                if z.Demurrage_amount.currency == Money(1, 'USD').currency:
                    if demurrage.exchange_rate:
                        y = .09 * (((float(z.Demurrage_amount.amount)) * demurrage.exchange_rate) + float(
                            (z.damage_amount.amount)))
                        TaxTotal += y
                        Taxamount += y
                    else:
                        TaxTotal = "------"
                else:
                    y = .09 * (((float(z.Demurrage_amount.amount))) + float(
                        (z.damage_amount.amount)))
                    TaxTotal += y
                    Taxamount += y

            else:
                TaxTotal = "------"

    else:
        TaxTotal = ''

    currency_list = []
    if len(container) > 0:
        if demurrage.demo_F:
            for currency in Container.objects.filter(demurrage=demurrage).all():
                currency_list.append(currency.Demurrage_amount.currency)
            if Money(1, 'USD').currency in currency_list:
                if demurrage.exchange_rate:
                    Total = invoiceamount + Taxamount - float(demurrage.deposit.amount) - float(
                        demurrage.payment.amount)
                else:
                    Total = "-------"

            else:
                Total = invoiceamount + Taxamount - float(demurrage.deposit.amount) - float(
                    demurrage.payment.amount)
        else:
            Total = invoiceTotal


    else:
        Total = ''

    context = {}
    context['demurrage'] = demurrage
    context['container'] = container
    context['p_settings'] = p_settings
    context['invoiceTotal'] = (f"{invoiceTotal:,} ریال") if type(invoiceTotal) is float else invoiceTotal
    context['TaxTotal'] = (f"{TaxTotal:,} ریال") if type(TaxTotal) is float else TaxTotal

    if type(Total) is float and Total >= 0:
        context['Total'] = (f"{Total:,} ریال (بدهکار)")
    elif type(Total) is float and Total < 0:
        context['Total'] = (f"{Total:,} ریال (بستانکار)")
    else:
        context['Total'] = Total

    # The name of your PDF file
    if demurrage.shipment:
        filename = 'demo_{}'.format(demurrage.shipment.HBL_number)
    else:
        filename = 'demo_{}'.format(demurrage.uniqueId)

    # HTML FIle to be converted to PDF - inside your Django directory
    template = get_template('invoice/demo-pdf-template.html')

    # Render the HTML
    html = template.render(context)

    # Options - Very Important [Don't forget this]
    options = {
        'encoding': 'UTF-8',
        'javascript-delay': '1',  # Optional
        'orientation': 'landscape',
        'page-size': 'A4',
        'load-error-handling': 'ignore',
        'quiet': None,
        'enable-local-file-access': True,
        'header-spacing': 5,
        'footer-spacing': 5,
        ''
        'custom-header': [
            ('Accept-Encoding', 'gzip')
        ],
    }
    # Javascript delay is optional

    # Remember that location to wkhtmltopdf
    config_path = 'C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe'
    config = pdfkit.configuration(wkhtmltopdf=config_path)

    # IF you have CSS to add to template
    css1 = os.path.join(settings.CSS_LOCATION, 'assets', 'css', 'bootstrap.min.css')
    css2 = os.path.join(settings.CSS_LOCATION, 'assets', 'css', 'dashboard.css')

    # Create the file
    file_content = pdfkit.from_string(html, False, configuration=config, options=options)

    # Create the HTTP Response
    response = HttpResponse(file_content, content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename = {}'.format(filename)

    # Return
    return response


def emailDocumentDemoInvoice(request, slug):
    # fetch that invoice
    try:
        demurrage = Demurrage.objects.get(slug=slug)
        pass
    except:
        messages.error(request, 'Something went wrong')
        return redirect('invoice:demo-invoices')

    # fetch all the products - related to this invoice
    container = Container.objects.filter(demurrage=demurrage)

    # Get Client Settings
    p_settings = Settings.objects.filter(sales=request.user)

    # Calculate the Invoice Total
    invoiceamount = 0.0
    invoiceTotal = 0.0
    Taxamount = 0.0
    TaxTotal = 0.0
    Total = 0.0

    if len(container) > 0:
        for x in container:
            if demurrage.demo_F:

                if x.Demurrage_amount.currency == Money(1, 'USD').currency:
                    if demurrage.exchange_rate:
                        y = ((float(x.Demurrage_amount.amount)) * demurrage.exchange_rate) + float(
                            (x.damage_amount.amount))
                        invoiceTotal += y
                        invoiceamount += y
                    else:
                        invoiceTotal = "------"
                else:
                    y = (float(x.Demurrage_amount.amount)) + float((x.damage_amount.amount))
                    invoiceTotal += y
                    invoiceamount += y
            else:
                currency_list = []
                for currency in Container.objects.filter(demurrage=demurrage).all():
                    currency_list.append(currency.Demurrage_amount.currency)
                if Money(1, 'USD').currency in currency_list and Money(1, 'IRR').currency in currency_list:
                    invoiceTotal = "-----"
                else:
                    y = (x.Demurrage_amount)
                    invoiceTotal += y
    else:
        invoiceTotal = ''

    if len(container) > 0:
        for z in container:
            if demurrage.demo_F:
                if z.Demurrage_amount.currency == Money(1, 'USD').currency:
                    if demurrage.exchange_rate:
                        y = .09 * (((float(z.Demurrage_amount.amount)) * demurrage.exchange_rate) + float(
                            (z.damage_amount.amount)))
                        TaxTotal += y
                        Taxamount += y
                    else:
                        TaxTotal = "------"
                else:
                    y = .09 * (((float(z.Demurrage_amount.amount))) + float(
                        (z.damage_amount.amount)))
                    TaxTotal += y
                    Taxamount += y

            else:
                TaxTotal = "------"

    else:
        TaxTotal = ''

    currency_list = []
    if len(container) > 0:
        if demurrage.demo_F:
            for currency in Container.objects.filter(demurrage=demurrage).all():
                currency_list.append(currency.Demurrage_amount.currency)
            if Money(1, 'USD').currency in currency_list:
                if demurrage.exchange_rate:
                    Total = invoiceamount + Taxamount - float(demurrage.deposit.amount) - float(
                        demurrage.payment.amount)
                else:
                    Total = "-------"

            else:
                Total = invoiceamount + Taxamount - float(demurrage.deposit.amount) - float(
                    demurrage.payment.amount)
        else:
            Total = invoiceTotal


    else:
        Total = ''

    context = {}
    context['demurrage'] = demurrage
    context['container'] = container
    context['p_settings'] = p_settings
    context['invoiceTotal'] = (f"{invoiceTotal:,} ریال") if type(invoiceTotal) is float else invoiceTotal
    context['TaxTotal'] = (f"{TaxTotal:,} ریال") if type(TaxTotal) is float else TaxTotal

    if type(Total) is float and Total >= 0:
        context['Total'] = (f"{Total:,} ریال (بدهکار)")
    elif type(Total) is float and Total < 0:
        context['Total'] = (f"{Total:,} ریال (بستانکار)")
    else:
        context['Total'] = Total

    if demurrage.shipment:
        filename = 'demo_{}.pdf'.format(demurrage.shipment.HBL_number)
    else:
        filename = 'demo_{}.pdf'.format(demurrage.uniqueId)

    # HTML FIle to be converted to PDF - inside your Django directory
    template = get_template('invoice/demo-pdf-template.html')

    # Render the HTML
    html = template.render(context)

    # Render the HTML

    # Options - Very Important [Don't forget this]
    options = {
        'encoding': 'UTF-8',
        'javascript-delay': '1',  # Optional
        'orientation': 'landscape',
        'page-size': 'A4',
        'load-error-handling': 'ignore',
        'quiet': None,
        'enable-local-file-access': True,
        'header-spacing': 5,
        'footer-spacing': 5,
        ''
        'custom-header': [
            ('Accept-Encoding', 'gzip')
        ],
    }
    # Javascript delay is optional

    # Remember that location to wkhtmltopdf
    config_path = 'C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe'
    config = pdfkit.configuration(wkhtmltopdf=config_path)
    # Saving the File
    filepath = os.path.join(settings.MEDIA_ROOT, 'demo_invoices\\demurrage')
    os.makedirs(filepath, exist_ok=True)
    pdf_save_path = filepath + filename
    # Save the PDF
    pdfkit.from_string(html, pdf_save_path, configuration=config, options=options)

    # send the emails to client
    to_email = demurrage.shipment.reference_number.companyName.email
    from_client = "HUYAR TARABAR CO."
    emailInvoiceClient(to_email, from_client, pdf_save_path)

    demurrage.status = 'EMAIL_SENT'
    demurrage.save()

    # Email was send, redirect back to view - invoice
    messages.success(request, "Email sent to the client succesfully")
    return redirect('invoice:create-build-demo-invoice', slug=slug)


def deletecontainer(request, slug):
    try:
        cont = Container.objects.get(slug=slug)
        demurrage_slug = cont.demurrage.slug
        Container.objects.get(slug=slug).delete()

    except:
        cont = Container.objects.get(slug=slug)
        demurrage_slug = cont.demurrage.slug
        messages.error(request, 'Something went wrong')

        return redirect('invoice:demo-createBuildDemoInvoice', slug=demurrage_slug)
    messages.success(request, "The selected container was removed.")
    return redirect("invoice:create-build-demo-invoice", slug=demurrage_slug)
