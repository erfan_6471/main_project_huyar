from django.urls import path, re_path

from contact_list.views import ContactList, ContactCreate, ContactUpdate, ContactListDetail, ContactDetailCreate, \
    ContactDetailUpdate, ContactProfileDetailView, search_contact_list, search_contact_detail, search_by_category

app_name = "contact"

urlpatterns = [path("contactlist/", ContactList.as_view(), name="contact_list"),
               re_path(r'^search/$', search_contact_list, name='search_contact_list'),
               path('create/company', ContactCreate.as_view(), name='contact-create'),
               path('update/company/<int:pk>', ContactUpdate.as_view(), name='contact_update'),
               path('detail/<slug:slug>', ContactListDetail.as_view(), name="contact_detail_list"),
               re_path(r'^search_detail/$', search_contact_detail, name='search_contact_detail'),
               path('category/<slug:slug>', search_by_category.as_view(), name="category"),
               path('create/contact_detail', ContactDetailCreate.as_view(), name='contact_detail_create'),
               path('update/contact_detail/<int:pk>', ContactDetailUpdate.as_view(), name='contact_detail_update'),
               path('detail/profile/<int:pk>', ContactProfileDetailView.as_view(),
                    name="contact_detail_profile"),
               ]
