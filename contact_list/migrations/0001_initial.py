# Generated by Django 4.0.1 on 2022-01-28 17:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import phonenumber_field.modelfields
import shortuuidfield.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', shortuuidfield.fields.ShortUUIDField(blank=True, editable=False, max_length=22, unique=True, verbose_name='کد اختصاصی')),
                ('slug', models.SlugField(max_length=100, unique=True, verbose_name='اسلاگ شرکت')),
                ('company_name', models.CharField(max_length=30, verbose_name='نام شرکت')),
                ('created_on', models.DateField(auto_now_add=True, verbose_name='تاریخ ایجاد')),
                ('creator', models.ForeignKey(limit_choices_to={'is_active': True}, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='ایجاد کننده')),
            ],
            options={
                'verbose_name': 'شرکت حمل ',
                'verbose_name_plural': 'شرکت های حمل',
            },
        ),
        migrations.CreateModel(
            name='DetailContact',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=30, null=True, verbose_name='نام')),
                ('last_name', models.CharField(max_length=30, verbose_name='نام خانوادگی')),
                ('phone_type', models.CharField(choices=[('home', 'Home'), ('mobile', 'Mobile'), ('fax', 'Fax'), ('work', 'Work'), ('other', 'Other')], max_length=10, verbose_name='نوع شماره تماس')),
                ('phone_number', phonenumber_field.modelfields.PhoneNumberField(max_length=50, region=None, verbose_name='شماره تفن')),
                ('email_type', models.CharField(choices=[('private', 'private'), ('work', 'Work'), ('other', 'Other')], max_length=10, verbose_name='نوع ایمیل')),
                ('email_address', models.EmailField(db_index=True, max_length=255, verbose_name='آدرس ایمیل')),
                ('kind_shipment', models.CharField(choices=[('TT', 'TT'), ('CC', 'CC'), ('AA', 'AA'), ('TR', 'TR')], max_length=2, verbose_name='حیطه فعالیت')),
                ('is_active', models.BooleanField(default=True, verbose_name='آیا این شخص  فغال می باشد ؟')),
                ('date_added', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('author', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator_contact', to=settings.AUTH_USER_MODEL, verbose_name='ایجاد کننده')),
                ('contact_object', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='contact_phone_number', to='contact_list.contact', verbose_name='شرکت در حال فعالت')),
            ],
            options={
                'verbose_name': 'اطلاعات تماس با شرکت های حمل ',
                'verbose_name_plural': 'اطلاعات تماس  با شرکت های حمل',
                'db_table': 'contacts_detail',
                'ordering': ['-is_active', 'date_modified'],
            },
        ),
    ]
