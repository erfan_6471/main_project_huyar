##################
class FieldsContactDetailMixin():

    def dispatch(self, request, *args, **kwargs):
        self.fields = ["first_name", "last_name", "phone_type", "phone_number", "email_type", "email_address",
                       "contact_object", "kind_shipment", "category", "is_active"]
        if request.user.is_superuser:
            self.fields.append("author")

        return super().dispatch(request, *args, **kwargs)


class FormContactDetailValiMixin():
    def form_valid(self, form):
        if self.request.user.is_superuser:
            form.save()
        else:
            self.obj = form.save(commit=False)
            self.obj.author = self.request.user
        return super().form_valid(form)
