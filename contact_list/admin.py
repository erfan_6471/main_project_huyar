from django.contrib import admin

from contact_list.models import Contact, DetailContact, Category

admin.site.register(Contact)
admin.site.register(DetailContact)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ("title", "position", "slug", "status")
    list_filter = (("status",))
    search_fields = ("title", "slug")
    prepopulated_fields = {"slug": ("title",)}


admin.site.register(Category, CategoryAdmin)
