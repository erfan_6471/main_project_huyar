import django_filters
from django import forms

from account.models import User
from contact_list.models import Contact, DetailContact


class ContactFilterList(django_filters.FilterSet):
    company_name = django_filters.CharFilter(lookup_expr='icontains', label=" نام شرکت")
    year_joined = django_filters.NumberFilter(field_name='created_on', lookup_expr='year', label="سال تاریخ ایجاد")
    year_joined__gt = django_filters.NumberFilter(field_name='created_on', lookup_expr='year__gte',
                                                  label="ایجاد بعد از این تاریخ")
    year_joined__lt = django_filters.NumberFilter(field_name='created_on', lookup_expr='year__lte',
                                                  label="ایجاد قبل از این تاریخ")

    class Meta:
        model = Contact
        fields = ['company_name', 'creator', 'year_joined']


class ContactFilterDetail(django_filters.FilterSet):
    last_name = django_filters.CharFilter(lookup_expr='icontains', label=" نام خانوادگی")
    email_address = django_filters.CharFilter(lookup_expr='icontains', label=" آدرس ایمیل")
    year_joined = django_filters.NumberFilter(field_name='date_added', lookup_expr='year', label="سال تاریخ ایجاد")
    year_joined__gt = django_filters.NumberFilter(field_name='date_added', lookup_expr='year__gte',
                                                  label="ایجاد بعد از این تاریخ")
    year_joined__lt = django_filters.NumberFilter(field_name='date_added', lookup_expr='year__lte',
                                                  label="ایجاد قبل از این تاریخ")
    author = django_filters.ModelMultipleChoiceFilter(queryset=User.objects.filter(is_sales=True),
                                                      widget=forms.CheckboxSelectMultiple)

    class Meta:
        model = DetailContact
        fields = ['first_name', 'last_name', 'email_address', 'author', 'kind_shipment', 'is_active', 'contact_object','category']
