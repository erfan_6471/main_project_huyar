from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404, render
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView
from django_otp.decorators import otp_required
from two_factor.views import OTPRequiredMixin

from account.mixin import AuthenticatedLogin, FieldsContactMixin, FormContactValiMixin
from contact_list.filters import ContactFilterList, ContactFilterDetail
from contact_list.mixin import FieldsContactDetailMixin, FormContactDetailValiMixin
from contact_list.models import Contact, DetailContact, Category


class ContactList(OTPRequiredMixin, AuthenticatedLogin, LoginRequiredMixin, ListView):
    template_name = "registration/contact_list.html"
    context_object_name = "contacts"
    queryset = Contact.objects.all()
    paginate_by = 10


@otp_required
@login_required
def search_contact_list(request):
    contact_list = Contact.objects.all()
    contact_filter = ContactFilterList(request.GET, queryset=contact_list)
    return render(request, "registration/contact_search.html", {'filter': contact_filter})


class ContactCreate(OTPRequiredMixin, AuthenticatedLogin, LoginRequiredMixin, SuccessMessageMixin, FieldsContactMixin,
                    FormContactValiMixin, CreateView):
    model = Contact
    template_name = "registration/contact-create-update.html"
    success_url = reverse_lazy("contact:contact_list")
    success_message = "تغییرات با موفقیت ایجاد شد"


class ContactUpdate(OTPRequiredMixin, LoginRequiredMixin, AuthenticatedLogin, SuccessMessageMixin, FieldsContactMixin,
                    FormContactValiMixin, UpdateView):
    model = Contact
    template_name = "registration/contact-create-update.html"
    success_url = reverse_lazy("contact:contact_list")
    success_message = "تغییرات با موفقیت ایجاد شد"


# company_detail
class ContactListDetail(OTPRequiredMixin, AuthenticatedLogin, LoginRequiredMixin, ListView):
    def get_queryset(self):
        slug = self.kwargs.get("slug")
        detail_contacts = DetailContact.objects.filter(contact_object__slug=slug)
        return detail_contacts

    paginate_by = 10
    template_name = "registration/contact_detail_list.html"


@otp_required
@login_required
def search_contact_detail(request):
    contact_list = DetailContact.objects.all()
    contact_filter = ContactFilterDetail(request.GET, queryset=contact_list)
    return render(request, "registration/contact_detail_search.html", {'filter': contact_filter})


class search_by_category(OTPRequiredMixin, AuthenticatedLogin, LoginRequiredMixin, ListView):
    template_name = "registration/contact_detail_tag.html"
    context_object_name = "contacts"

    def get_queryset(self):
        global category
        slug = self.kwargs.get("slug")
        category = get_object_or_404(Category.objects.active(), slug=slug)
        return category.contact_list.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = category
        return context


class ContactDetailCreate(OTPRequiredMixin, SuccessMessageMixin, AuthenticatedLogin, LoginRequiredMixin,
                          FieldsContactDetailMixin,
                          FormContactDetailValiMixin,
                          CreateView):
    model = DetailContact
    template_name = "registration/contact-detail-create-update.html"
    success_url = reverse_lazy("contact:contact_list")
    success_message = "تغییرات با موفقیت ایجاد شد"



class ContactDetailUpdate(OTPRequiredMixin, SuccessMessageMixin, AuthenticatedLogin, LoginRequiredMixin,
                          FieldsContactDetailMixin,
                          FormContactDetailValiMixin,
                          UpdateView):
    model = DetailContact
    template_name = "registration/contact-detail-create-update.html"
    success_message = "تغییرات با موفقیت ایجاد شد"
    def get_success_url(self):
        return reverse_lazy("contact:contact_detail_profile", kwargs={'pk': self.kwargs['pk']})


class ContactProfileDetailView(OTPRequiredMixin, AuthenticatedLogin, LoginRequiredMixin, DetailView):
    context_object_name = "contact"

    def get_object(self, queryset=None):
        pk = self.kwargs.get("pk")
        return get_object_or_404(DetailContact, pk=pk)

    template_name = "registration/contact_detail_profile.html"
