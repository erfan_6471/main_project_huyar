from django.urls import path

from mailbox2.views import compose, email, mailbox, index, deleteEmail, Inbox_list, Sent_list, Inbox_Detail, Sent_Detail

app_name = "mail"

urlpatterns = [
    # API Routes
    path("", index, name="index"),
    path("emails", compose, name="compose"),
    path("emails/<int:email_id>", email, name="email"),
    path("emails/<str:mailbox>", mailbox, name="mailbox"),
    path('emails/delete/<int:id>', deleteEmail, name='delete-email'),
    path("Inbox_list/", Inbox_list.as_view(), name="inbox_list"),
    path("sent_list/", Sent_list.as_view(), name="sent_list"),
    path('mail_detail/<int:pk>', Inbox_Detail.as_view(), name="inbox_detail"),
    path('sent_detail/<int:pk>', Sent_Detail.as_view(), name="sent_detail"),
]
