from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from account.models import User
from comment.models import Comment


class Email(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="emails", verbose_name="فرستنده ")
    sender = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name="emails_sent", verbose_name="گیرنده")
    recipients = models.ManyToManyField(User, related_name="emails_received")
    subject = models.CharField(max_length=255, verbose_name="موضوع")
    body = models.TextField(verbose_name="متن پیام")
    timestamp = models.DateTimeField(auto_now_add=True, verbose_name="زمان دریافت")
    read = models.BooleanField(default=False, verbose_name="وضعیت بررسی")
    archived = models.BooleanField(default=False, verbose_name="آرشیو")
    comments = GenericRelation(Comment)

    def checkbox(self):
        if self.read:
            return "ایمیل خوانده شده"
        else:
            return "ایمیل خوانده نشده "

    def serialize(self):
        return {
            "id": self.id,
            "sender": self.sender.email,
            "recipients": [user.email for user in self.recipients.all()],
            "subject": self.subject,
            "body": self.body,
            "timestamp": self.timestamp.strftime("%b %d %Y, %I:%M %p"),
            "read": self.read,
            "archived": self.archived
        }

    def __str__(self):
        return f"From: {self.sender}, Sub: {self.subject}"
