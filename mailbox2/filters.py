import django_filters

from mailbox2.models import Email


class EmailFilterList(django_filters.FilterSet):
    timestamp = django_filters.DateRangeFilter(label="زمان ایجاد")

    class Meta:
        model = Email
        fields = ['sender', 'recipients', 'subject', 'timestamp', 'body', 'read', 'archived']
