import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse, HttpResponse
from django.shortcuts import HttpResponseRedirect, render, redirect, get_object_or_404
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, DetailView

from account.models import User
from client_list.mixin import ClientListMixin
from two_factor.views import OTPRequiredMixin
from .filters import EmailFilterList
from .models import Email


def index(request):
    # Authenticated users view their inbox
    if request.user.is_authenticated:
        return render(request, "mail/inbox.html")

    # Everyone else is prompted to sign in
    else:
        return HttpResponseRedirect(reverse("login"))


@csrf_exempt
@login_required
def compose(request):
    # Composing a new email must be via POST
    if request.method != "POST":
        return JsonResponse({"error": "POST request required."}, status=400)

    # Check recipient emails
    data = json.loads(request.body)
    emails = [email.strip() for email in data.get("recipients").split(",")]
    if emails == [""]:
        return JsonResponse({
            "error": "At least one recipient required."
        }, status=400)

    # Convert email addresses to users
    recipients = []
    for email in emails:
        try:
            user = User.objects.get(email=email)
            recipients.append(user)
        except User.DoesNotExist:
            return JsonResponse({
                "error": f"کاربری با ایمل  {email} یافت نشد.",
                "status": 400
            }, status=400)

    # Get contents of email
    subject = data.get("subject")
    body = data.get("body")

    # Create one email for each recipient, plus sender
    users = set()
    users.add(request.user)
    users.update(recipients)
    for user in users:
        email = Email(
            user=user,
            sender=request.user,
            subject=subject,
            body=body,
            read=user == request.user
        )
        email.save()
        for recipient in recipients:
            email.recipients.add(recipient)
        email.save()

    return JsonResponse({"message": "ایمیل با موفقیت ارسال شد .", "status": 201}, status=201)


@login_required
def mailbox(request, mailbox):
    # Filter emails returned based on mailbox
    if mailbox == "inbox":
        emails = Email.objects.filter(
            user=request.user, recipients=request.user, read=False
        )
    elif mailbox == "sent":
        emails = Email.objects.filter(
            user=request.user, sender=request.user
        )
    elif mailbox == "archive":
        emails = Email.objects.filter(
            user=request.user, recipients=request.user, read=True, archived=True
        )
    else:
        return JsonResponse({"error": "Invalid mailbox."}, status=400)

    # Return emails in reverse chronologial order
    emails = emails.order_by('read', '-timestamp')[:20].all()
    return JsonResponse([email.serialize() for email in emails], safe=False)


@csrf_exempt
@login_required
def email(request, email_id):
    # Query for requested email
    try:
        email = Email.objects.get(user=request.user, pk=email_id)
    except Email.DoesNotExist:
        return JsonResponse({"error": "Email not found."}, status=404)

    # Return email contents
    if request.method == "GET":
        return JsonResponse(email.serialize())

    # Update whether email is read or should be archived
    elif request.method == "PUT":
        data = json.loads(request.body)
        if data.get("read") is not None:
            email.read = data["read"]
        if data.get("archived") is not None:
            email.archived = data["archived"]
        email.save()
        return HttpResponse(status=204)

    # Email must be via GET or PUT
    else:
        return JsonResponse({
            "error": "GET or PUT request required."
        }, status=400)


class Inbox_list(OTPRequiredMixin, ClientListMixin, LoginRequiredMixin, ListView):
    def get_queryset(self):
        inbox_mail = Email.objects.filter(user=self.request.user, recipients=self.request.user)
        inbox_filter = EmailFilterList(self.request.GET, queryset=inbox_mail)
        return inbox_filter

    template_name = "mail/mail_list.html"
    context_object_name = "filter"


class Sent_list(OTPRequiredMixin, ClientListMixin, LoginRequiredMixin, ListView):
    def get_queryset(self):
        sent_mail = Email.objects.filter(user=self.request.user, sender=self.request.user)
        sent_filter = EmailFilterList(self.request.GET, queryset=sent_mail)
        return sent_filter

    template_name = "mail/sent_list.html"
    context_object_name = "filter"


class Inbox_Detail(OTPRequiredMixin, ClientListMixin, LoginRequiredMixin, DetailView):

    def get_object(self, queryset=None):
        pk = self.kwargs.get("pk")
        return get_object_or_404(Email.objects.filter(user=self.request.user), pk=pk)

    template_name = "mail/Inbox_detail.html"
    context_object_name = "email"


class Sent_Detail(OTPRequiredMixin, ClientListMixin, LoginRequiredMixin, DetailView):

    def get_object(self, queryset=None):
        pk = self.kwargs.get("pk")
        return get_object_or_404(Email.objects.filter(sender=self.request.user), pk=pk)

    template_name = "mail/sent_detail.html"
    context_object_name = "email"


def deleteEmail(request, id):
    try:
        Email.objects.get(id=id).delete()
        messages.success(request, 'پیام با موفقت حذف گردید.')
    except:
        messages.error(request, 'Something went wrong')
        return redirect('mail:inbox_list')

    return redirect('mail:inbox_list')
