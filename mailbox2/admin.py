from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from mailbox2.models import Email


class MailboxAdmin(SummernoteModelAdmin):
    list_display = ("sender", "subject", "timestamp", "read")
    list_filter = (("timestamp", "read",))
    search_fields = ("subject", "body", "sender", "recipients")
    ordering = ("read", "-timestamp")
    body = '__all__'


admin.site.register(Email, MailboxAdmin)
