from django.apps import AppConfig


class Mailbox2Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mailbox2'
