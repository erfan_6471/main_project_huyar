from django.urls import path

from shipment_list.views import Shipment_List, Shipment_Detail, ShipmentCreate, ShipmentUpdate

app_name = "shipment"

urlpatterns = [
    path("shipmentlist/", Shipment_List.as_view(), name="shipment_list"),
    path('shipment/detail/<int:pk>', Shipment_Detail.as_view(), name="shipment_detail"),
    path('shipment/create', ShipmentCreate.as_view(), name='shipment-create'),
    path('shipment/update/<int:pk>', ShipmentUpdate.as_view(), name='shipment-update'),
]
