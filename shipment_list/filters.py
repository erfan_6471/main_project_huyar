import django_filters

from shipment_list.models import ShipmentList


class ShipmentFilter(django_filters.FilterSet):
    shipment_number = django_filters.CharFilter(lookup_expr='icontains', label=" شماره سفر")
    reference_number__reference_number = django_filters.CharFilter(lookup_expr='icontains', label=" شماره رفرنس")
    BL_number = django_filters.CharFilter(lookup_expr='icontains', label=" شماره بارنامه")
    truck_number = django_filters.CharFilter(lookup_expr='icontains', label=" شماره ماشین")
    container_number = django_filters.CharFilter(lookup_expr='icontains')
    created = django_filters.DateRangeFilter()
    year_joined__gt = django_filters.NumberFilter(field_name='created', lookup_expr='year__gte',
                                                  label="ایجاد بعد از این سال")
    year_joined__lt = django_filters.NumberFilter(field_name='created', lookup_expr='year__lte',
                                                  label="ایجاد قبل از این سال")

    class Meta:
        model = ShipmentList
        fields = ['shipment_number', 'reference_number__reference_number', 'BL_number', 'truck_number',
                  'container_number', 'created',
                  'kind_shipment', 'reference_number__companyName__sales']
