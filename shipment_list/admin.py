from django.contrib import admin

from shipment_list.models import ShipmentList


class ShipmentListAdmin(admin.ModelAdmin):
    list_display = (
        "shipment_number", "reference_number", "MBL_number", "created")
    list_filter = (("created",))
    search_fields = (
        "BL_number", "shipment_number",)
    ordering = ("-created", "shipment_number", "reference_number")


admin.site.register(ShipmentList, ShipmentListAdmin)
