from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView
from two_factor.views import OTPRequiredMixin

from shipment_list.filters import ShipmentFilter
from shipment_list.mixin import DocumentLogin, AuthorShipmentAccessMixin, DocumentCreate, DocumentUpdate
from shipment_list.models import ShipmentList


class Shipment_List(OTPRequiredMixin, DocumentLogin, LoginRequiredMixin, ListView):
    def get_queryset(self):
        if self.request.user.is_superuser or self.request.user.is_account or self.request.user.is_document:
            shipment_list = ShipmentList.objects.all()
        else:
            shipment_list = ShipmentList.objects.filter(reference_number__companyName__sales=self.request.user)

        shipment_filter = ShipmentFilter(self.request.GET, queryset=shipment_list)
        return shipment_filter

    template_name = "registration/shipment_list.html"
    context_object_name = "filter"


class Shipment_Detail(OTPRequiredMixin, DocumentLogin, AuthorShipmentAccessMixin, LoginRequiredMixin, DetailView):

    def get_object(self, queryset=None):
        pk = self.kwargs.get("pk")
        return get_object_or_404(ShipmentList.objects.all(), pk=pk)

    template_name = "registration/shipment_detail.html"
    context_object_name = "shipment"


class ShipmentCreate(OTPRequiredMixin, SuccessMessageMixin, DocumentCreate, DocumentLogin, LoginRequiredMixin,
                     CreateView):
    model = ShipmentList
    fields = ["shipment_number", "reference_number", "BL_number", "kind_shipment", "container_number", "truck_number",
              "MBL_status",
              "MBL_file", "HBL_status", "HBL_file", "WR_invoice", "WR_invoice_payment", "DO_invoice",
              "DO_invoice_payment",
              "Transit_invoice", "Transit_invoice_payment", "other_charge", "other_charge_payment",
              "freight_charge_payment", "ETA",
              "description"]
    template_name = "registration/shipment-create-update.html"
    success_url = reverse_lazy("shipment:shipment_list")
    success_message = " یک سفر جدید با موفقیت ایجاد شد."


class ShipmentUpdate(OTPRequiredMixin, SuccessMessageMixin, DocumentUpdate, DocumentLogin, LoginRequiredMixin,
                     UpdateView):
    model = ShipmentList
    fields = ["shipment_number", "reference_number", "BL_number", "kind_shipment", "container_number", "truck_number",
              "MBL_status",
              "MBL_file", "HBL_status", "HBL_file", "WR_invoice", "WR_invoice_payment", "DO_invoice",
              "DO_invoice_payment",
              "Transit_invoice", "Transit_invoice_payment", "other_charge", "other_charge_payment",
              "freight_charge_payment", "ETA",
              "description"]
    template_name = "registration/shipment-create-update.html"
    success_url = reverse_lazy("shipment:shipment_list")
    success_message = " تغییرات با موفقیت ذخیره شد."
