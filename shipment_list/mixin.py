from django.http import Http404
from django.shortcuts import redirect, get_object_or_404

from shipment_list.models import ShipmentList


class DocumentLogin():
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.is_document or request.user.is_account or request.user.is_sales or request.user.is_superuser:
                return super().dispatch(request, *args, **kwargs)
            else:
                return redirect("account:profile")
        else:
            return redirect("login")


class AuthorShipmentAccessMixin():
    def dispatch(self, request, pk, *args, **kwargs):
        shipment = get_object_or_404(ShipmentList, pk=pk)
        if shipment.reference_number.companyName.sales == request.user or request.user.is_superuser \
                or request.user.is_document or request.user.is_account:
            return super().dispatch(request, *args, **kwargs)
        else:
            raise Http404("you can't see this page.")


class DocumentCreate():
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.is_document:
                return super().dispatch(request, *args, **kwargs)
            else:
                return redirect("account:profile")
        else:
            return redirect("login")


class DocumentUpdate():
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.is_document or request.user.is_account:
                return super().dispatch(request, *args, **kwargs)
            else:
                return redirect("account:profile")
        else:
            return redirect("login")