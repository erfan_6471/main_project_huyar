import secrets
import uuid

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.db.models import CharField
from django.template.defaultfilters import slugify
from django.utils import timezone
from django_mysql.models import ListCharField
from djmoney.models.fields import MoneyField

from comment.models import Comment
from extensions.utils import Jalali_covertor
from reference_list.models import ReferenceList


class ShipmentList(models.Model):
    STATUS_CHOICES = (('OR', 'بارنامه اورجینال می باشد.'),  # orginal
                      ('TR', 'بارنامه تلکس می باشد.'),  # telext
                      ('ORD', 'بارنامه اورجینال و تحویل اسناد داده شده است.'),  # orginal and delivered
                      ('TBL', 'بار بصورت زمینی حمل می شود و بارنامه دریایی ندارد.')
                      )

    SHIPMENT_CHOICES = (('TT', 'TT'),  # truck
                        ('CC', 'CC'),  # ocean
                        ('AA', 'AA'),  # air
                        ('TR', 'TR'),  # transit
                        ('SC', 'SC'),)  # transit
    shipment_number = models.CharField(verbose_name="شماره سفر", max_length=20, unique=True)
    reference_number = models.OneToOneField(ReferenceList, on_delete=models.CASCADE,
                                            related_name="reference_list",
                                            verbose_name="شماره رفرنس",
                                            limit_choices_to={'status': 'p'})
    kind_shipment = models.CharField(verbose_name="نوع سفر", max_length=2, choices=SHIPMENT_CHOICES)
    MBL_number = models.CharField(verbose_name="شماره بارنامه دریایی",blank=True,null=True, max_length=20, unique=True)
    HBL_number = models.CharField(verbose_name="شماره بارنامه مشتری",blank=True,null=True, max_length=20, unique=True)
    tracking = models.CharField(unique=True, default=uuid.uuid4().hex[:12], editable=False, max_length=250)
    container_number = ListCharField(verbose_name="شماره کانتینر ها",
                                     base_field=CharField(max_length=11),
                                     size=6,
                                     max_length=(20 * 12)  # 6 * 10 character nominals, plus commas
                                     )
    truck_number = models.CharField(verbose_name="شماره کامیون", max_length=20, null=True,
                                    blank=True)
    MBL_status = models.CharField(verbose_name="وضعیت بارنامه دریایی", max_length=3, choices=STATUS_CHOICES)
    MBL_file = models.FileField(verbose_name='تصویر بارنامه دریایی', upload_to='static/media/BL/MBL/', null=True,
                                blank=True)
    HBL_status = models.CharField(verbose_name="وضعیت بارنامه فورواردری", max_length=3, choices=STATUS_CHOICES)
    HBL_file = models.FileField(verbose_name='تصویر بارنامه فورواردری', upload_to='static/media/BL/HBL/', null=True,
                                blank=True)
    created = models.DateTimeField(verbose_name="زمان ایجاد سفر", auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    WR_invoice = MoneyField(max_digits=14, null=True, blank=True, decimal_places=2, default_currency='IRR',
                            verbose_name="صورتحساب  قبض انبار")
    WR_invoice_payment = models.BooleanField(verbose_name="آیا صورتحساب قبض انبار پرداخت شده است؟  ", default=False)
    DO_invoice = MoneyField(max_digits=14, null=True, blank=True, decimal_places=2, default_currency='IRR',
                            verbose_name="صورتحساب ترخیصیه")
    DO_invoice_payment = models.BooleanField(verbose_name="آیا صورتحساب ترخیصیه پرداخت شده است؟  ", default=False)
    Transit_invoice = MoneyField(max_digits=14, null=True, blank=True, decimal_places=2, default_currency='IRR',
                                 verbose_name="صورتحساب ترانزیت")
    Transit_invoice_payment = models.BooleanField(verbose_name="آیا صورتحساب ترانزیت پرداخت شده است؟  ", default=False)
    other_charge = MoneyField(max_digits=14, null=True, blank=True, decimal_places=2, default_currency='IRR',
                              verbose_name="صورتحساب غیر رسمی")
    other_charge_payment = models.BooleanField(verbose_name="آیا صورتحساب متفرقه پرداخت شده است؟  ", default=False)
    freight_charge_payment = models.BooleanField(verbose_name="آیا صورتحساب کرایه حمل پرداخت شده است؟  ", default=False)
    ETA = models.DateField(verbose_name="زمان تقریبی رسیدن ", default=timezone.now)
    description = models.CharField(max_length=1000, null=True, blank=True, verbose_name="توضیحات", )
    uniqueId = models.CharField(null=True, blank=True, max_length=100)
    comments = GenericRelation(Comment)

    class Meta:
        verbose_name = " سفر "
        verbose_name_plural = " سفرها"
        ordering = ["-created", "kind_shipment"]

    def __str__(self):
        return self.shipment_number

    def save(self, *args, **kwargs):
        if self.created is None:
            self.date_created = timezone.localtime(timezone.now())
        if self.uniqueId is None:
            self.uniqueId = str(uuid.uuid4()).split('-')[4]
            self.slug = slugify('{} {}'.format(self.shipment_number, self.uniqueId))

        self.slug = slugify('{} {}'.format(self.shipment_number, self.uniqueId))
        self.last_updated = timezone.localtime(timezone.now())

        super(ShipmentList, self).save(*args, **kwargs)

    def Jpublish(self):
        return Jalali_covertor(self.reference_number.start_project)

    def tracking_code(self):
        return secrets.token_hex(5)

    Jpublish.short_description = "زمان شروع حمل"

    def period_time(self):
        time = self.ETA.day - timezone.now().day
        return time
