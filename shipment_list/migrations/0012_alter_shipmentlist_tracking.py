# Generated by Django 4.0.1 on 2022-04-26 18:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shipment_list', '0011_alter_shipmentlist_tracking'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shipmentlist',
            name='tracking',
            field=models.CharField(default='e0d2b92f460c', editable=False, max_length=250, unique=True),
        ),
    ]
