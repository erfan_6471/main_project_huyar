# Generated by Django 4.0.1 on 2022-05-08 19:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shipment_list', '0024_alter_shipmentlist_tracking'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shipmentlist',
            name='tracking',
            field=models.CharField(default='b3ae029ff0a4', editable=False, max_length=250, unique=True),
        ),
    ]
