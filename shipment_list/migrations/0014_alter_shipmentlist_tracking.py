# Generated by Django 4.0.1 on 2022-05-06 16:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shipment_list', '0013_alter_shipmentlist_tracking'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shipmentlist',
            name='tracking',
            field=models.CharField(default='50c2977f917e', editable=False, max_length=250, unique=True),
        ),
    ]
