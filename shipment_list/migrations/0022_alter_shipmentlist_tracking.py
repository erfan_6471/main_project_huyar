# Generated by Django 4.0.1 on 2022-05-08 18:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shipment_list', '0021_alter_shipmentlist_tracking'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shipmentlist',
            name='tracking',
            field=models.CharField(default='33e55d06f959', editable=False, max_length=250, unique=True),
        ),
    ]
