# Generated by Django 4.0.1 on 2022-06-05 16:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shipment_list', '0043_alter_shipmentlist_tracking'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shipmentlist',
            name='tracking',
            field=models.CharField(default='9da19142964e', editable=False, max_length=250, unique=True),
        ),
    ]
