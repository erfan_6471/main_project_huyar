# Generated by Django 4.0.1 on 2022-05-31 20:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shipment_list', '0037_alter_shipmentlist_tracking'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shipmentlist',
            name='tracking',
            field=models.CharField(default='2b8df31afddd', editable=False, max_length=250, unique=True),
        ),
    ]
