from django.apps import AppConfig


class ShipmentListConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'shipment_list'
    verbose_name = "سفرها"
