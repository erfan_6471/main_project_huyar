from datetime import datetime, timedelta

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import render
from django.views import View
from online_users.models import OnlineUserActivity

from account.models import User
from calendar_event.models import Event
from client_list.models import ClientList
from contact_list.models import Contact, DetailContact
from mailbox2.models import Email
from reference_list.models import ReferenceList
from todo.models import Task


@login_required
def site_slide_bar_partial(request):
    emails = Email.objects.filter(user=request.user, recipients=request.user, read=False).count()
    context = {
        'count': emails
    }
    return render(request, "registration/sidebar.html", context)


@login_required
def event_slide_bar_partial(request):
    your_events = Event.objects.get_upcoming_events(user=request.user)
    my_events = Event.objects.get_running_events(user=request.user)
    tasks = Task.objects.filter(completed=False, assigned_to=request.user)
    counts = your_events.count() + my_events.count() + tasks.count()
    context = {
        'your_events': your_events,
        'my_events': my_events,
        'tasks': tasks,
        'counts': counts
    }
    return render(request, "registration/tasks-tab.html", context)


def count_slide_bar_partial(request):
    your_events = Event.objects.get_upcoming_events(user=request.user)
    my_events = Event.objects.get_running_events(user=request.user)
    tasks = Task.objects.filter(completed=False, assigned_to=request.user)
    counts = your_events.count() + my_events.count() + tasks.count()
    context = {
        'counts': counts
    }
    return render(request, "registration/tasks-count.html", context)


last_month = datetime.today() - timedelta(days=30)
last_week = datetime.today() - timedelta(days=7)


@login_required()
def Index(request):
    references_investigation = ReferenceList.objects.filter(status="i").count()
    references_confirm_not_complete = ReferenceList.objects.filter(companyName__sales=request.user, status="p").count()
    references_confirm_complete = ReferenceList.objects.filter(companyName__sales=request.user, status="e").count()
    references_finished = ReferenceList.objects.filter(companyName__sales=request.user, status="f").count()
    references_not_confirm = ReferenceList.objects.filter(companyName__sales=request.user, status="d").count()
    references_all = ReferenceList.objects.filter(companyName__sales=request.user).count()
    references_new = ReferenceList.objects.filter(companyName__sales=request.user, created__gte=last_week).count()
    client_active = ClientList.objects.filter(sales=request.user, status=True).count()
    client_not_active = ClientList.objects.filter(sales=request.user, status=False).count()
    client_new = ClientList.objects.filter(sales=request.user, date_created__gte=last_month).count()
    client_new_week = ClientList.objects.filter(sales=request.user, date_created__gte=last_month).count()
    client_all = ClientList.objects.filter(sales=request.user).count()
    contact_list = Contact.objects.all().count()
    user_list = User.objects.filter(Q(is_sales=True) | Q(is_document=True) | Q(is_superuser=True) | Q(is_account=True))
    user_activity_objects = OnlineUserActivity.get_user_activities()
    emails = Email.objects.filter(
        user=request.user, recipients=request.user, archived=False
    )[0:10:-1]
    labels = []
    data = []

    queryset_1 = ReferenceList.objects.filter(companyName__sales=request.user, status="e").order_by("-created")
    for profit_rate in queryset_1:
        labels.append(f"{profit_rate.reference_number} / {profit_rate.net_rate_1.currency}")
        if profit_rate.net_rate_1.currency == profit_rate.selling_rate_1.currency and profit_rate.selling_rate_1.amount > 0:
            x = (profit_rate.selling_rate_1 - profit_rate.net_rate_1) / 4
            data.append(float(x.amount))
        else:
            x = "واحد پولی باید یکسان باشد"
            data.append(x)

    labels_2 = []
    data_2 = []

    queryset_2 = ReferenceList.objects.filter(companyName__sales=request.user, status="e")
    for profit_rate in queryset_2:
        labels_2.append(f"{profit_rate.reference_number} / {profit_rate.net_rate_2.currency}")
        if profit_rate.net_rate_2.currency == profit_rate.selling_rate_2.currency and profit_rate.selling_rate_2.amount > 0:
            x = (profit_rate.selling_rate_2 - profit_rate.net_rate_2) / 4
            data_2.append(float(x.amount))
        else:
            x = "واحد پولی باید یکسان باشد"
            data_2.append(x)

    labels_3 = []
    data_3 = []

    sales = User.objects.filter(is_sales=True)
    references_active = ReferenceList.objects.filter(status="p")
    for user in sales:
        labels_3.append(user.get_full_name())
        data_3.append(references_active.filter(companyName__sales=user).count())

    labels_4 = []
    data_4 = []

    sales = User.objects.filter(is_sales=True)
    references_active = ReferenceList.objects.filter(status="f", created__gte=datetime.now() - timedelta(days=365))
    tasks = Task.objects.filter(completed=False, assigned_to=request.user)
    for user in sales:
        labels_4.append(user.get_full_name())
        data_4.append(references_active.filter(companyName__sales=user).count())

    labels_5 = []
    data_5 = []

    sales = User.objects.filter(is_sales=True)
    contact_active = DetailContact.objects.filter(is_active=True)
    for user in sales:
        labels_5.append(user.get_full_name())
        data_5.append(contact_active.filter(author=user).count())

    labels_6 = []
    data_6 = []

    sales = User.objects.filter(is_sales=True)
    client_activate = ClientList.objects.filter(status=True)
    for user in sales:
        labels_6.append(user.get_full_name())
        data_6.append(client_activate.filter(sales=user).count())

    labels_7 = []
    data_7 = []

    commissions = ReferenceList.objects.filter(status="f", created__gte=datetime.now() - timedelta(days=365))
    for profit_rate in commissions:
        labels_7.append(list((profit_rate.created.strftime('%Y-%m-%d'), profit_rate.reference_number)))
        data_7.append(float((
                profit_rate.selling_rate_1.amount - profit_rate.net_rate_1.amount)))
    labels_7.reverse()
    data_7.reverse()

    labels_8 = []
    data_8 = []

    commissions = ReferenceList.objects.filter(status="f", created__gte=datetime.now() - timedelta(days=365))
    for profit_rate in commissions:
        labels_8.append(list((profit_rate.created.strftime('%Y-%m-%d'), profit_rate.reference_number)))
        data_8.append(float((
                profit_rate.selling_rate_2.amount - profit_rate.net_rate_2.amount)))
    labels_8.reverse()
    data_8.reverse()

    context = {
        'references_investigation': references_investigation,
        'references_confirm_not_complete': references_confirm_not_complete,
        'references_confirm_complete': references_confirm_complete,
        'references_finished': references_finished,
        'references_not_confirm': references_not_confirm,
        'references_all': references_all,
        'client_active': client_active,
        'client_not_active': client_not_active,
        'client_new': client_new,
        'client_all': client_all,
        'labels_1': labels,
        'data_1': data,
        'queryset_1': queryset_1,
        'labels_2': labels_2,
        'data_2': data_2,
        'queryset_2': queryset_2,
        'labels_3': labels_3,
        'data_3': data_3,
        'labels_4': labels_4,
        'data_4': data_4,
        'labels_5': labels_5,
        'data_5': data_5,
        'labels_6': labels_6,
        'data_6': data_6,
        'labels_7': labels_7,
        'data_7': data_7,
        'labels_8': labels_8,
        'data_8': data_8,
        'references_new': references_new,
        'client_new_week': client_new_week,
        'contact_list': contact_list,
        'user_list': user_list,
        'user_activity_objects': user_activity_objects,
        'emails': emails,
        'tasks': tasks,

    }
    return render(request, "registration/home.html", context)


class DashboardView(LoginRequiredMixin, View):
    login_url = "accounts:signin"
    template_name = "calendar/calendarapp/dashboard.html"

    def get(self, request, *args, **kwargs):
        events = Event.objects.get_all_events(user=request.user)
        running_events = Event.objects.get_running_events(user=request.user)
        latest_events = Event.objects.filter(user=request.user).order_by("-id")[:10]
        context = {
            "total_event": events.count(),
            "running_events": running_events,
            "latest_events": latest_events,
        }
        return render(request, self.template_name, context)
