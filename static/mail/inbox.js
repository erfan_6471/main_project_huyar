document.addEventListener(
    "DOMContentLoaded",
    function () {
        const form = document.querySelector("#compose-form");
        const msg = document.querySelector("#message");
        form.addEventListener("submit", (event) => {
            event.preventDefault();
            to = document.querySelector("#compose-recipients");
            subject = document.querySelector("#compose-subject");
            body = document.querySelector("#compose-body");
            if (from.length == 0 && to.length == 0) return;

            fetch("/mail/emails", {
                method: "POST",
                body: JSON.stringify({
                    recipients: to.value,
                    subject: subject.value,
                    body: body.value,
                }),
            })
                .then((response) => response.json())
                .then((result) => {
                    console.log(result.status);
                    if (result.status == 201) {
                        load_mailbox("sent");
                    } else {
                        msg.innerHTML = `<div class="alert alert-danger" role="alert">
            ${result.error}
          </div>`;
                    }
                });
        });
    },
    false
);

document.addEventListener("DOMContentLoaded", function () {
    // Use buttons to toggle between views
    document.querySelector("#inbox").addEventListener("click", () => load_mailbox("inbox"));
    document.querySelector("#sent").addEventListener("click", () => load_mailbox("sent"));
    document.querySelector("#archived").addEventListener("click", () => load_mailbox("archive"));
    document.querySelector("#compose").addEventListener("click", compose_email);

    // By default, load the inbox
    load_mailbox("inbox");
});

function compose_email() {
    // Show compose view and hide other views
    document.querySelector("#emails-view").style.display = "none";
    document.querySelector("#compose-view").style.display = "block";

    // Clear out composition fields
    document.querySelector("#compose-recipients").value = "";
    document.querySelector("#compose-subject").value = "";
    document.querySelector("#compose-body").value = "";
}

function load_mailbox(mailbox) {
    // Show the mailbox and hide other views
    document.querySelector("#emails-view").style.display = "block";
    document.querySelector("#compose-view").style.display = "none";

    // Show the mailbox name
    document.querySelector("#emails-view").innerHTML = `<h3>${mailbox.charAt(0).toUpperCase() + mailbox.slice(1)}</h3>`;

    if (mailbox == "show_mail") {
        show_mail();
        return;
    }

    fetch(`/mail/emails/${mailbox}`)
        .then((response) => response.json())
        .then((emails) => {
            emails.forEach((element) => {
                if (mailbox != "sent") {
                    sender_recipients = element.sender;
                } else {
                    sender_recipients = element.recipients;
                }
                if (mailbox == "inbox") {
                    if (element.read) is_read = "read";
                    else is_read = "";
                }
                var item = document.createElement("div");
                item.className = `card ${is_read} my-1 items`;
                item.innerHTML = `<div  style="background-color:#dff0d8;margin: 20px; padding:5px;border-radius: 15px;border: 1px solid rgba(0,0,0,0);box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px, rgba(11,31,52,0.35) 0px -2px 6px 0px inset;" id="item-${element.id}">


          
        
          پیام دریافتی از :<strong style="margin-right: 10px;">${sender_recipients}</strong>  <div style="margin-right: 10px; font-size: small">${element.subject}</div> 
        <div style="margin-left: 10px; float: left; font-size: small">${element.timestamp}</div>
        <div style="margin-left: 10px; float: left; font-size: small">${element.read}</div>
       
        <br>
      </div>`;
                document.querySelector("#emails-view").appendChild(item);
                item.addEventListener("click", () => {
                    show_mail(element.id, mailbox);
                });
            });
        });
}

function show_mail(id, mailbox) {
    fetch(`/mail/emails/${id}`)
        .then((response) => response.json())
        .then((email) => {
            // Print email
            // console.log(email);
            document.querySelector("#emails-view").innerHTML = "";
            var item = document.createElement("div");
            item.innerHTML = `<div style="white-space: pre-wrap;">
  <strong>از:</strong> ${email.sender}
  <strong>به:</strong> ${email.recipients}
  <strong>موضوع:</strong> ${email.subject}
  <strong>زمان دریافت:</strong> ${email.timestamp}
  </div>
  `;
            document.querySelector("#emails-view").appendChild(item);
            if (mailbox == "sent") return;
            let archive = document.createElement("btn");
            archive.className = `btn btn-sm btn-danger`;
            archive.addEventListener("click", () => {
                toggle_archive(id, email.archived);
                if (archive.innerText == "Archive") archive.innerText = "از آرشیو در آوردن";
                else archive.innerText = "آرشیو کردن";
            });
            if (!email.archived) archive.textContent = "آرشیو کردن";
            else archive.textContent = "از آرشیو در آوردن";
            document.querySelector("#emails-view").appendChild(archive);

            let reply = document.createElement("btn");
            reply.className = `btn btn-sm btn-danger`;
            reply.textContent = "پاسخ";
            reply.addEventListener("click", () => {
                reply_mail(email.sender, email.subject, email.body, email.timestamp);
            });
            document.querySelector("#emails-view").appendChild(reply);
            make_read(id);
            let hr = document.createElement("HR");
            document.querySelector("#emails-view").appendChild(hr);
            let message = document.createElement("div");
            message.innerHTML = ` <div  style="background-color:ghostwhite;margin: 20px; padding:15px;border-radius: 3px;border: 10px solid rgba(0,0,0,0);box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(80,79,79,0.3) 0px 30px 60px -30px, rgba(11,31,52,0.35) 0px -2px 6px 0px inset;">
 ${email.body}</div>`;
            document.querySelector("#emails-view").appendChild(message);
        });
}

function toggle_archive(id, state) {
    fetch(`/mail/emails/${id}`, {
        method: "PUT",
        body: JSON.stringify({
            archived: !state,
        }),
    });
}

function make_read(id) {
    fetch(`/mail/emails/${id}`, {
        method: "PUT",
        body: JSON.stringify({
            read: true,
        }),
    });
}

function reply_mail(sender, subject, body, timestamp) {
    compose_email();
    if (!/^در پاسخ:/.test(subject)) subject = `در پاسخ: ${subject}`;
    document.querySelector("#compose-recipients").value = sender;
    document.querySelector("#compose-subject").value = subject;

    pre_fill = `On ${timestamp} ${sender} wrote:\n${body}\n`;

    document.querySelector("#compose-body").value = pre_fill;
}