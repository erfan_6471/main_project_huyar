from django import forms

from client_list.models import ClientList
from reference_list.models import ReferenceList


class ReferenceForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(ReferenceForm, self).__init__(*args, **kwargs)
        self.fields['companyName'] = forms.ModelChoiceField(queryset=ClientList.objects.filter(sales=user),
                                                            label="نام شرکت")

    class Meta:
        model = ReferenceList
        fields = ["reference_number", "companyName", "kind_shipment", "start_project", "position", "description",
                  "country_of_receipt", "port_of_receipt", "country_of_discharge", "port_of_discharge", "net_rate_1",
                  "selling_rate_1","net_rate_2","selling_rate_2", "status"]
