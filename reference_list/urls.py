from django.urls import path

from reference_list.views import Reference_List, ReferenceCreate, ReferenceUpdate, Reference_Detail, pie_chart_1, \
    pie_chart_2, Reference_search_List

app_name = "reference"

urlpatterns = [
    path("referencelist/", Reference_List.as_view(), name="reference_list"),
    path("search/", Reference_search_List.as_view(), name='search_reference_list'),
    path('reference/create', ReferenceCreate.as_view(), name='reference-create'),
    path('reference/update/<int:pk>', ReferenceUpdate.as_view(), name='reference-update'),
    path('reference/detail/<int:pk>', Reference_Detail.as_view(), name="reference_detail"),
    path('profit_chart/', pie_chart_1, name='pie_chart_1'),
    path('profit_chart_2/', pie_chart_2, name='pie_chart_2'),
]
