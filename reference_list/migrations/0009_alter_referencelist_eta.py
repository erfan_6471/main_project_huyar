
from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('reference_list', '0008_referencelist_carrier'),
    ]

    operations = [
        migrations.AlterField(
            model_name='referencelist',
            name='ETA',
            field=models.DateField(default=django.utils.timezone.now, verbose_name='زمان تقریبی رسیدن '),
        ),
    ]
