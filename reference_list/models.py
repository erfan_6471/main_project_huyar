from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.utils import timezone
from django_countries.fields import CountryField
from djmoney.models.fields import MoneyField

from client_list.models import ClientList
from comment.models import Comment
from contact_list.models import Contact
from extensions.utils import Jalali_covertor


class ReferenceList(models.Model):
    STATUS_CHOICES = (('d', 'تایید نشده'),  # not confirm
                      ('i', 'در حال بررسی'),  # investigation
                      ('p', 'تایید و در حال انجام '),  # confirm and not complete
                      ('e', 'تایید و به پایان رسیده ( سود دریافت نشده ) '),  # confirm and complete (not paid)
                      ('f', 'بایگانی'),  # finished
                      )
    SHIPMENT_CHOICES = (('TT', 'TT'),  # truck
                        ('CC', 'CC'),  # ocean
                        ('AA', 'AA'),  # air
                        ('TR', 'TR'),  # transit
                        ('SC', 'SC'),)  # transit
    STATUS_BL = (('PR', 'prepaid'),  #
                 ('CO', 'collect'),)  # transit
    reference_number = models.CharField(verbose_name="شماره رفرنس", max_length=20, unique=True)
    companyName = models.ForeignKey(ClientList, null=True, on_delete=models.CASCADE,
                                    related_name="reference_list",
                                    verbose_name="مشتری",
                                    limit_choices_to={'status': True})
    carrier = models.ForeignKey(Contact, null=True, on_delete=models.CASCADE,
                                related_name="contact",
                                verbose_name="شرکت حمل کننده")
    kind_shipment = models.CharField(verbose_name="نوع سفر", max_length=2, choices=SHIPMENT_CHOICES)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    start_project = models.DateField(verbose_name="زمان شروع حمل", default=timezone.now)
    position = models.IntegerField(verbose_name="اولویت")
    country_of_receipt = CountryField(verbose_name="کشور بارگیری محموله")
    port_of_receipt = models.CharField(verbose_name="پورت بارگیری محموله", max_length=200)
    country_of_discharge = CountryField(verbose_name="کشور مقصد")
    port_of_discharge = models.CharField(verbose_name="گمرک مقصد", max_length=200)
    status_MBL = models.CharField(default="CO", verbose_name="وضعیت پرداخت بارنامه MBL", max_length=2,
                                  choices=STATUS_BL)
    status_HBL = models.CharField(default="CO", verbose_name="وضعیت پرداخت بارنامه HBL", max_length=2,
                                  choices=STATUS_BL)
    net_rate_1 = MoneyField(max_digits=14, null=True, decimal_places=2, default_currency='USD',
                            verbose_name="نرخ خرید ارزی")
    selling_rate_1 = MoneyField(max_digits=14, null=True, decimal_places=2, default_currency='USD',
                                verbose_name="نرخ فروش ارزی")
    net_rate_2 = MoneyField(max_digits=14, null=True, decimal_places=2, default_currency='IRR',
                            verbose_name="نرخ خرید ریالی")
    selling_rate_2 = MoneyField(max_digits=14, null=True, decimal_places=2, default_currency='IRR',
                                verbose_name="نرخ فروش ریالی")
    ETA = models.DateField(verbose_name="زمان تقریبی رسیدن ", default=timezone.now)
    status = models.CharField(verbose_name="وضعیت رفرنس", max_length=1, choices=STATUS_CHOICES)
    description = models.CharField(max_length=1000, null=True, blank=True, verbose_name="توضیحات", )
    comments = GenericRelation(Comment)

    class Meta:
        verbose_name = "رفرنس بازاریاب "
        verbose_name_plural = "رفرنس های بازاریاب"
        ordering = ["-created"]

    def __str__(self):
        return self.reference_number

    def profit_1(self):
        if self.net_rate_1.currency == self.selling_rate_1.currency:
            if float(self.selling_rate_1.amount) - float(self.net_rate_1.amount) > 0:
                x = (self.selling_rate_1 - self.net_rate_1) / 4
            else:
                x = (self.selling_rate_1 - self.net_rate_1)
            return x
        else:
            return "واحد پول باید یکسان باشد"

    def profit_2(self):
        if self.net_rate_2.currency == self.selling_rate_2.currency:
            if float(self.selling_rate_2.amount) - float(self.net_rate_2.amount) > 0:
                z = (self.selling_rate_2 - self.net_rate_2) / 4
            else:
                z = (self.selling_rate_2 - self.net_rate_2)
            return z
        else:
            return "واحد پول باید یکسان باشد"

    def Jpublish(self):
        return Jalali_covertor(self.start_project)

    Jpublish.short_description = "زمان شروع حمل"

    def period_time(self):
        time = timezone.now().day - self.start_project.day
        return time
