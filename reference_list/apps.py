from django.apps import AppConfig


class ReferenceListConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'reference_list'
    verbose_name = "لیست رفرنس ها"

