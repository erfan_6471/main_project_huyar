from django.http import Http404
from django.shortcuts import redirect, get_object_or_404

from reference_list.models import ReferenceList


class SalesLogin():
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.is_sales or request.user.is_superuser:
                return super().dispatch(request, *args, **kwargs)
            else:
                return redirect("account:profile")
        else:
            return redirect("two_factor:login")


class AuthorReferenceAccessMixin():
    def dispatch(self, request, pk, *args, **kwargs):
        reference = get_object_or_404(ReferenceList, pk=pk)
        if reference.companyName.sales == request.user or request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        else:
            raise Http404("you can't see this page.")


class FieldsReferenceMixin():

    def dispatch(self, request, *args, **kwargs):
        self.fields = ["reference_number", "kind_shipment", "start_project", "position", "carrier",
                       "country_of_receipt", "port_of_receipt", "country_of_discharge", "port_of_discharge",
                       "status_MBL", "status_HBL","ETA",
                       "net_rate_1", "selling_rate_1", "net_rate_2", "selling_rate_2", "status", "description"]
        if request.user.is_superuser:
            self.fields.append("companyName")

        return super().dispatch(request, *args, **kwargs)
