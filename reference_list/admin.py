from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from reference_list.models import ReferenceList


class ReferenceListAdmin(SummernoteModelAdmin):
    list_display = (
        "reference_number", "companyName", "kind_shipment", "country_of_receipt", "port_of_discharge")
    list_filter = (("status", "kind_shipment", "start_project",))
    search_fields = ("companyName", "firstName", "lasttName")
    ordering = ("-kind_shipment", "-position", "-start_project")
    summernote_fields = '__all__'





admin.site.register(ReferenceList, ReferenceListAdmin)
