import django_filters

from reference_list.models import ReferenceList


class ReferenceFilter(django_filters.FilterSet):
    shipment_number = django_filters.CharFilter(lookup_expr='icontains', label=" شماره سفر")
    reference_number = django_filters.CharFilter(lookup_expr='icontains', label=" شماره رفرنس")
    created = django_filters.DateRangeFilter()
    year_joined__gt = django_filters.NumberFilter(field_name='created', lookup_expr='year__gte',
                                                  label="ایجاد بعد از این سال")
    year_joined__lt = django_filters.NumberFilter(field_name='created', lookup_expr='year__lte',
                                                  label="ایجاد قبل از این سال")

    class Meta:
        model = ReferenceList
        fields = ['reference_number', 'companyName', 'kind_shipment', 'country_of_receipt', 'port_of_receipt', 'port_of_discharge',
                  'status','created']
