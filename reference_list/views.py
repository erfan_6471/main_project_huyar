from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404, render
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DetailView
from django_otp.decorators import otp_required

from account.models import User
from reference_list.filters import ReferenceFilter
from reference_list.forms import ReferenceForm
from reference_list.mixin import SalesLogin, AuthorReferenceAccessMixin, FieldsReferenceMixin
from reference_list.models import ReferenceList
from two_factor.views import OTPRequiredMixin


class Reference_List(OTPRequiredMixin, SalesLogin, LoginRequiredMixin, ListView):

    def get_queryset(self):
        if self.request.user.is_superuser:
            return ReferenceList.objects.all()
        else:
            return ReferenceList.objects.filter(companyName__sales=self.request.user)

    template_name = "registration/reference_list.html"
    context_object_name = "references"
    paginate_by = 10


class Reference_search_List(OTPRequiredMixin, SalesLogin, LoginRequiredMixin, ListView):

    def get_queryset(self):
        if self.request.user.is_superuser:
            reference_list = ReferenceList.objects.all()
        else:
            reference_list = ReferenceList.objects.filter(companyName__sales=self.request.user)

        reference_filter = ReferenceFilter(self.request.GET, queryset=reference_list)
        return reference_filter

    template_name = "registration/reference_search_list.html"
    context_object_name = "filter"


class ReferenceCreate(OTPRequiredMixin, SalesLogin, LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = ReferenceList
    form_class = ReferenceForm
    template_name = "registration/reference-create-update.html"

    def get_object(self, queryset=None):
        return User.objects.get(pk=self.request.user.pk)

    def get_form_kwargs(self):
        kwargs = super(ReferenceCreate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    success_url = reverse_lazy("reference:reference_list")
    success_message = "تغییرات با موفقیت ایجاد شد"


class ReferenceUpdate(OTPRequiredMixin, LoginRequiredMixin, AuthorReferenceAccessMixin, FieldsReferenceMixin,
                      SuccessMessageMixin, UpdateView):
    model = ReferenceList
    template_name = "registration/reference-create-update.html"
    success_url = reverse_lazy("reference:reference_list")
    success_message = "تغییرات با موفقیت ایجاد شد"


class Reference_Detail(OTPRequiredMixin, SalesLogin, AuthorReferenceAccessMixin, LoginRequiredMixin, DetailView):

    def get_object(self, queryset=None):
        pk = self.kwargs.get("pk")
        return get_object_or_404(ReferenceList.objects.all(), pk=pk)

    template_name = "registration/reference_detail.html"
    context_object_name = "reference"


@otp_required
@login_required
def pie_chart_1(request):
    labels = []
    data = []

    queryset_1 = ReferenceList.objects.filter(companyName__sales=request.user, status="e")
    for profit_rate in queryset_1:
        labels.append(f"{profit_rate.reference_number} / {profit_rate.net_rate_1.currency}")
        if profit_rate.net_rate_1.currency == profit_rate.selling_rate_1.currency and profit_rate.selling_rate_1.amount > 0:
            if (float(profit_rate.selling_rate_1.amount) - float(profit_rate.net_rate_1.amount)) > 0:
                x = (profit_rate.selling_rate_1 - profit_rate.net_rate_1) / 4
            else:
                x = (profit_rate.selling_rate_1 - profit_rate.net_rate_1)
            data.append(float(x.amount))
        else:
            x = "واحد پولی باید یکسان باشد"
            data.append(x)

    return render(request, 'registration/profit_monthly.html', {
        'labels_1': labels,
        'data_1': data,
        'queryset_1': queryset_1})


@otp_required
@login_required
def pie_chart_2(request):
    labels_1 = []
    data_1 = []
    queryset_2 = ReferenceList.objects.filter(companyName__sales=request.user, status="e")
    for profit_rate in queryset_2:
        labels_1.append(f"{profit_rate.reference_number} / {profit_rate.net_rate_2.currency}")
        if profit_rate.net_rate_2.currency == profit_rate.selling_rate_2.currency and profit_rate.selling_rate_2.amount > 0:
            if (float(profit_rate.selling_rate_2.amount) - float(profit_rate.net_rate_2.amount)) > 0:
                x = (profit_rate.selling_rate_2 - profit_rate.net_rate_2) / 4
            else:
                x = (profit_rate.selling_rate_2 - profit_rate.net_rate_2)
            data_1.append(float(x.amount))
        else:
            x = "واحد پولی باید یکسان باشد"
            data_1.append(x)

    return render(request, 'registration/profit_monthly_2.html', {
        'labels_2': labels_1,
        'data_2': data_1,
        'queryset_2': queryset_2})
